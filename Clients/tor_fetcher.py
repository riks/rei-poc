import logging
import asyncio

import aiohttp

from rei import tor, messages, FetcherHost, BaseFetcher


class TorFetcher(BaseFetcher):
    async def __ainit__(
        self,
        connector,
        *args,
        index=None,
        loop=None,
        **kwargs,
    ):
        self._loop = loop or asyncio.get_event_loop()

        self._connector = connector

        self._active = False

        await super().__ainit__(*args, index=index, loop=self._loop, **kwargs)

        self._name = "#{0}".format(super().name)

        self._session = aiohttp.ClientSession(
            headers={
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; " +
                              "x64; rv:59.0) Gecko/20100101 Firefox/59.0",
            },
            connector=self._connector,
            raise_for_status=False,
        )

    @property
    def name(self):
        if self.alias:
            return "{0}#{1}".format(self.alias, super().name)
        else:
            return "#{0}".format(super().name)

    async def bad_client(self, message):
        await super().bad_client(message)

        await asyncio.sleep(3600)

        self._active = False

    async def fetch(self, message):
        try:
            result = await self._fetch(message.url)

            if result["status_code"] < 300:
                self._log.info(
                    "%s: Resource %r fetched",
                    self.name,
                    result["url"],
                )
            else:
                self._log.error(
                    "%s: Resource %r failed",
                    self.name,
                    result["url"],
                )

            response = messages.FetchResponse(
                id=message.id,
                source=self.id,
                destination=message.source,
                alias=message.alias,
                task_id=message.task_id,
                content_kind=message.content_kind,
                **result,
            )

            self.send_message(response)
        except Exception as exception:
            self._log.exception("%s: Fetcher error", self.name)

            response = messages.FetchResponse(
                id=message.id,
                source=self.id,
                destination=message.source,
                status_code=500,
                status_text="Internal Server Error",
                alias=message.alias,
                task_id=message.task_id,
                content_kind=message.content_kind,
                url=message.url,
            )

            self.send_message(response)

            if isinstance(exception, tor.TorError):
                self.end()

                self._active = False

    async def main(self):
        while self.websocket is not None and not self.websocket.closed:
            if self._id is None and not self._active:
                try:
                    await self._fetch("http://checkip.amazonaws.com/")
                except tor.TorError:
                    await asyncio.sleep(3600)

                    break
                except Exception:
                    await asyncio.sleep(1800)

                    continue

                await self.begin()

                self._active = True

            await asyncio.sleep(15)

        await self._session.close()

        await self._connector.close_circuit()

        self._connector.close()

    async def _fetch(self, url):
        async with self._session.get(url) as response:
            return {
                "url": url,
                "status_code": response.status,
                "status_text": response.reason,
                "content_type": response.content_type,
                "content": await response.read(),
            }


async def main(loop=None):
    loop = loop or asyncio.get_event_loop()

    host = await FetcherHost("http://localhost:8000/api", loop=loop)

    control = await tor.TorControl(("10.10.10.2", 9060), ("localhost", 9061))

    while True:
        connector = await control.connector()

        if connector is None:
            await asyncio.sleep(300)

            continue

        await host.spawn(TorFetcher, connector)

    await control.close()

if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        datefmt="%Y/%m/%d %H:%M:%S",
        format="%(asctime)s [%(levelname)s]: %(message)s",
    )

    loop = asyncio.get_event_loop()

    loop.run_until_complete(main(loop=loop))
