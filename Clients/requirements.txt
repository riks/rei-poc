aiohttp==2.3.10
aiosocks==0.2.6
asyncpg==0.15.0
html5lib==1.0.1
lxml==4.1.1
pycurl==7.43.0.1