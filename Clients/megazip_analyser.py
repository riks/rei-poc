import logging
import asyncio

from urllib.parse import urljoin

from lxml import etree, html
from lxml.html import html5parser

from rei import BaseAnalyser, ResponseError


XPATH_MODEL = etree.XPath(
    "//ul[contains(@class, 's-catalog__columns-list')]" +
    "/li[contains(@class, 'filtred_item')]" +
    "/a[contains(@class, 's-catalog__model-link')]/@href"
)

XPATH_VARIANT = etree.XPath(
    "//ul[contains(@class, 's-catalog__body-variants')]" +
    "/li[contains(@class, 's-catalog__body-variants-item')]"
)

XPATH_VARIANT_LINK = etree.XPath(
    ".//a[contains(@class, 's-catalog__body-variants-name')]/@href"
)

XPATH_VARIANT_IMAGE = etree.XPath(
    ".//div[contains(@class, 's-catalog__body-variants-item-image')]/img"
)

XPATH_PART_GROUP = etree.XPath(
    "//ul[contains(@class, 'part-group')]" +
    "/li[contains(@class, 'part-group__item')]"
)

XPATH_PART_GROUP_LINK = etree.XPath(
    ".//a[contains(@class, 'part-group__image-link')]/@href"
)

XPATH_PART_GROUP_IMAGE = etree.XPath(
    ".//a[contains(@class, 'part-group__image-link')]/img"
)

XPATH_CATALOG = etree.XPath(
    "//div[contains(@class, 's-catalog__items')]"
)

XPATH_CATALOG_IMAGE = etree.XPath(
    ".//div[contains(@class, 's-catalog__items-image-group')]/img"
)


class Analyser(BaseAnalyser):
    @property
    def delay(self):
        return 45

    def html(self, source):
        parser = html5parser.HTMLParser(namespaceHTMLElements=False)
        html_parser = html.HTMLParser()

        try:
            return html5parser.fromstring(source.content, parser=parser)
        except ValueError:
            try:
                return html.fromstring(source.content, parser=html_parser)
            except Exception:
                self.log.error("Resource %r invalid", source.url)

                raise
        except Exception:
            self.log.error("Resource %r invalid", source.url)

            raise

    async def handle_model_list(self, task):
        try:
            source = await self.fetch(task)
        except ResponseError as exception:
            if exception.status_code == 429:
                self.bad_fetcher()

            raise

        html = self.html(source)

        for item in XPATH_MODEL(html):
            url = urljoin(task.url, item)

            await self.ensure_task(
                task.alias,
                "variant_list",
                url,
                task.task_id,
            )

    async def handle_variant_list(self, task):
        try:
            source = await self.fetch(task)
        except ResponseError as exception:
            if exception.status_code == 429:
                self.bad_fetcher()

            raise

        html = self.html(source)

        items = XPATH_VARIANT(html)

        if not len(items):
            # TODO: Change content type here

            return

        for item in items:
            url = urljoin(task.url, XPATH_VARIANT_LINK(item)[0])

            await self.ensure_task(
                task.alias,
                "part_group_list",
                url,
                task.task_id,
            )

            img = XPATH_VARIANT_IMAGE(item)

            if len(img):
                img = img[0]

                src = img.attrib.get("src", None)
                srcset = img.attrib.get("srcset", None)
                data_srcset = img.attrib.get("data-srcset", None)

                if data_srcset is not None:
                    data_srcset = data_srcset.split()[0::2]

                    for src_url in data_srcset:
                        src_url = urljoin(task.url, src_url)

                        await self.ensure_task(
                            task.alias,
                            "variant_image",
                            src_url,
                            task.task_id,
                        )

                if srcset is not None:
                    srcset = srcset.split()[0::2]

                    for src_url in srcset:
                        src_url = urljoin(task.url, src_url)

                        await self.ensure_task(
                            task.alias,
                            "variant_image",
                            src_url,
                            task.task_id,
                        )

                if src is not None:
                    src = urljoin(task.url, src)

                    await self.ensure_task(
                        task.alias,
                        "variant_image",
                        src,
                        task.task_id,
                    )

    async def handle_variant_image(self, task):
        try:
            await self.fetch(task, delay=0)
        except ResponseError as exception:
            if exception.status_code == 429:
                self.bad_fetcher()

            if exception.status_code != 404:
                raise

    async def handle_part_group_list(self, task):
        try:
            source = await self.fetch(task)
        except ResponseError as exception:
            if exception.status_code == 429:
                self.bad_fetcher()

            raise

        html = self.html(source)

        items = XPATH_PART_GROUP(html)

        for item in items:
            url = urljoin(task.url, XPATH_PART_GROUP_LINK(item)[0])

            await self.ensure_task(
                task.alias,
                "catalog",
                url,
                task.task_id,
            )

        for item in items:
            img = XPATH_PART_GROUP_IMAGE(item)

            if len(img):
                img = img[0]

                src = img.attrib.get("src", None)
                srcset = img.attrib.get("srcset", None)
                data_srcset = img.attrib.get("data-srcset", None)

                if data_srcset is not None:
                    data_srcset = data_srcset.split()[0::2]

                    for src_url in data_srcset:
                        src_url = urljoin(task.url, src_url)

                        await self.ensure_task(
                            task.alias,
                            "part_group_image",
                            src_url,
                            task.task_id,
                        )

                if srcset is not None:
                    srcset = srcset.split()[0::2]

                    for src_url in srcset:
                        src_url = urljoin(task.url, src_url)

                        await self.ensure_task(
                            task.alias,
                            "part_group_image",
                            src_url,
                            task.task_id,
                        )

                if src is not None:
                    src = urljoin(task.url, src)

                    await self.ensure_task(
                        task.alias,
                        "part_group_image",
                        src,
                        task.task_id,
                    )

    async def handle_part_group_image(self, task):
        try:
            await self.fetch(task, delay=0)
        except ResponseError as exception:
            if exception.status_code == 429:
                self.bad_fetcher()

            if exception.status_code != 404:
                raise

    async def handle_catalog(self, task):
        try:
            source = await self.fetch(task)
        except ResponseError as exception:
            if exception.status_code == 429:
                self.bad_fetcher()

            raise

        html = self.html(source)

        catalog = XPATH_CATALOG(html)[0]

        img = XPATH_CATALOG_IMAGE(catalog)

        if len(img):
            img = img[0]

            src = img.attrib.get("src", None)
            srcset = img.attrib.get("srcset", None)
            data_srcset = img.attrib.get("data-srcset", None)

            if data_srcset is not None:
                data_srcset = data_srcset.split()[0::2]

                for src_url in data_srcset:
                    src_url = urljoin(task.url, src_url)

                    await self.ensure_task(
                        task.alias,
                        "catalog_image",
                        src_url,
                        task.task_id,
                    )

            if srcset is not None:
                srcset = srcset.split()[0::2]

                for src_url in srcset:
                    src_url = urljoin(task.url, src_url)

                    await self.ensure_task(
                        task.alias,
                        "catalog_image",
                        src_url,
                        task.task_id,
                    )

            if src is not None:
                src = urljoin(task.url, src)

                await self.ensure_task(
                        task.alias,
                        "catalog_image",
                        src,
                        task.task_id,
                    )

    async def handle_catalog_image(self, task):
        try:
            await self.fetch(task, delay=0)
        except ResponseError as exception:
            if exception.status_code == 429:
                self.bad_fetcher()

            if exception.status_code != 404:
                raise


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        datefmt="%Y/%m/%d %H:%M:%S",
        format="%(asctime)s [%(levelname)s]: %(message)s",
    )

    loop = asyncio.get_event_loop()

    loop.run_until_complete(Analyser.run(
        "http://localhost:8000/api",
        "megazip",
        concurrency=9,
    ))
