import time
import json
import base64
import random
import string


_message_types = {}


def message(message_type):
    def message(cls):
        cls.__message_type__ = message_type
        _message_types[message_type] = cls

        return cls

    return message


class ResponseError(Exception):
    __slots__ = ("_status_code", "_status_text")

    def __init__(self, status_code, status_text=None):
        self._status_code = status_code
        self._status_text = status_text

        if self._status_text:
            super().__init__(
                "{0} [{1}]".format(self._status_text, self._status_code))
        else:
            super().__init__("Unknown Error [{0}]".format(self._status_code))

    @property
    def status_code(self):
        return self._status_code

    @property
    def status_text(self):
        return self._status_text


class Message:
    __slots__ = (
        "__message_type__",
        "_id",
        "_timestamp",
        "source",
        "destination",
    )

    def __init__(
        self,
        *,
        id=None,
        source=None,
        destination=None,
        timestamp=None,
        **kwargs,
    ):
        self._id = id
        self._timestamp = timestamp

        self.source = source
        self.destination = destination

    @property
    def id(self):
        if not self._id:
            self._id = "#" +\
                "".join(random.choices(string.ascii_lowercase, k=15))

        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    def timestamp(self):
        if not self._timestamp:
            self._timestamp = time.time()

        return self._timestamp

    @property
    def type(self):
        assert(self.__message_type__)

        return self.__message_type__

    @timestamp.setter
    def timestamp(self, value):
        self._timestamp = value

    @classmethod
    def load(cls, source):
        message_type = source["type"]

        assert(message_type)

        if cls is Message:
            message_class = _message_types.get(message_type, None)

            if message_class:
                message = message_class(**source)

                assert(message.type == message_type)

                return message
            else:
                message = Message(**source)

                message.__message_type__ = message_type

                return message

        message = cls(**source)

        assert(message.type == message_type)

        return message

    def dump(self):
        result = {
            "id": self.id,
            "type": self.type,
            "timestamp": self.timestamp,
        }

        if self.source:
            result["source"] = self.source

        if self.destination:
            result["destination"] = self.destination

        return result

    @classmethod
    def from_json(cls, str):
        source = json.loads(str)

        return cls.load(source)

    def to_json(self):
        return json.dumps(self.dump())

    def __str__(self):
        return str(self.dump())

    def __repr__(self):
        values = sorted(self.dump().items())

        return "<{0} {1}>".format(
            self.__class__.__name__,
            " ".join(["{0}={1!r}".format(*item) for item in values]),
        )


@message("unlock_message")
class UnlockMessage(Message):
    __slots__ = ("task_id", "alias",)

    def __init__(self, *args, task_id=None, alias=None, **kwargs):
        assert(task_id is not None)
        assert(alias)

        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias

    def dump(self):
        result = super().dump()

        result["task_id"] = self.task_id
        result["alias"] = self.alias

        return result


@message("bad_client_message")
class BadClientMessage(Message):
    __slots__ = ()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


@message("bye_message")
class ByeMessage(Message):
    __slots__ = ()

    def __init__(self, *args, source=None, **kwargs):
        assert(source)

        super().__init__(*args, source=source, **kwargs)


class Request(Message):
    __slots__ = ()


@message("hello_request")
class HelloRequest(Request):
    __slots__ = ("client_type", "alias", "client_classes",)

    def __init__(
        self,
        *args,
        client_type=None,
        alias=None,
        client_classes=None,
        **kwargs,
    ):
        assert(client_type)

        super().__init__(*args, **kwargs)

        self.client_type = client_type
        self.alias = alias
        self.client_classes = client_classes

    def dump(self):
        result = super().dump()

        result["client_type"] = self.client_type

        if self.alias:
            result["alias"] = self.alias

        if self.client_classes:
            result["client_classes"] = self.client_classes

        return result


@message("analyse_request")
class AnalyseRequest(Request):
    __slots__ = ("task_id", "alias", "content_kind", "url", "parent_id")

    def __init__(
        self,
        *args,
        task_id=None,
        alias=None,
        content_kind=None,
        url=None,
        parent_id=None,
        **kwargs,
    ):
        assert(task_id is not None)
        assert(alias)
        assert(content_kind)
        assert(url)

        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias
        self.content_kind = content_kind
        self.url = url
        self.parent_id = parent_id

    def dump(self):
        result = super().dump()

        result["task_id"] = self.task_id
        result["alias"] = self.alias
        result["content_kind"] = self.content_kind
        result["url"] = self.url

        if self.parent_id is not None:
            result["parent_id"] = self.parent_id

        return result


@message("lock_request")
class LockRequest(Request):
    __slots__ = ("task_id", "alias",)

    def __init__(self, *args, task_id=None, alias=None, **kwargs):
        assert(task_id is not None)
        assert(alias)

        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias

    def dump(self):
        result = super().dump()

        result["task_id"] = self.task_id
        result["alias"] = self.alias

        return result


@message("fetch_request")
class FetchRequest(Request):
    __slots__ = (
        "task_id",
        "alias",
        "client_classes",
        "content_kind",
        "url",
        "parent_id",
    )

    def __init__(
        self,
        *args,
        task_id=None,
        alias=None,
        client_classes=None,
        content_kind=None,
        url=None,
        parent_id=None,
        **kwargs,
    ):
        assert(task_id is not None)
        assert(alias)
        assert(content_kind)
        assert(url)

        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias
        self.client_classes = client_classes
        self.content_kind = content_kind
        self.url = url
        self.parent_id = parent_id

    def dump(self):
        result = super().dump()

        result["task_id"] = self.task_id
        result["alias"] = self.alias

        if self.client_classes:
            result["client_classes"] = self.client_classes

        result["content_kind"] = self.content_kind
        result["url"] = self.url

        if self.parent_id is not None:
            result["parent_id"] = self.parent_id

        return result


@message("ensure_task_request")
class EnsureTaskRequest(Request):
    __slots__ = (
        "alias",
        "content_kind",
        "url",
        "parent_id",
    )

    def __init__(
        self,
        *args,
        alias=None,
        content_kind=None,
        url=None,
        parent_id=None,
        **kwargs,
    ):
        assert(alias)
        assert(content_kind)
        assert(url)

        super().__init__(*args, **kwargs)

        self.alias = alias
        self.content_kind = content_kind
        self.url = url
        self.parent_id = parent_id

    def dump(self):
        result = super().dump()

        result["alias"] = self.alias
        result["content_kind"] = self.content_kind
        result["url"] = self.url

        if self.parent_id is not None:
            result["parent_id"] = self.parent_id

        return result


class Response(Message):
    __slots__ = ("status_code", "status_text",)

    def __init__(self, *args, status_code=None, status_text=None, **kwargs):
        assert(status_code)

        super().__init__(*args, **kwargs)

        self.status_code = status_code
        self.status_text = status_text

    def dump(self):
        result = super().dump()

        result["status_code"] = self.status_code

        if self.status_text:
            result["status_text"] = self.status_text

        return result

    def raise_error(self):
        if self.status_code >= 400:
            raise ResponseError(self.status_code, self.status_text)


@message("hello_response")
class HelloResponse(Response):
    __slots__ = ()


@message("lock_response")
class LockResponse(Response):
    __slots__ = ("task_id", "alias", "locked",)

    def __init__(self, *args, task_id=None, alias=None, locked=None, **kwargs):
        assert(task_id is not None)
        assert(alias)
        assert(locked is not None)

        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias
        self.locked = locked

    def dump(self):
        result = super().dump()

        result["task_id"] = self.task_id
        result["alias"] = self.alias
        result["locked"] = self.locked

        return result


@message("analyse_response")
class AnalyseResponse(Response):
    __slots__ = ("task_id", "alias")

    def __init__(
        self,
        *args,
        task_id=None,
        alias=None,
        **kwargs,
    ):
        assert(task_id is not None)
        assert(alias)

        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias

    def dump(self):
        result = super().dump()

        result["task_id"] = self.task_id
        result["alias"] = self.alias

        return result


@message("fetch_response")
class FetchResponse(Response):
    __slots__ = (
        "task_id",
        "alias",
        "content_kind",
        "url",
        "content_type",
        "content",
    )

    def __init__(
        self,
        *args,
        task_id=None,
        alias=None,
        content_kind=None,
        url=None,
        content_type=None,
        content=None,
        **kwargs,
    ):
        assert(task_id is not None)
        assert(alias)
        assert(content_kind)
        assert(url)

        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias
        self.content_kind = content_kind
        self.url = url
        self.content_type = content_type

        if isinstance(content, str):
            self.content = base64.b64decode(content)
        else:
            self.content = content

    def dump(self):
        result = super().dump()

        result["task_id"] = self.task_id
        result["alias"] = self.alias
        result["content_kind"] = self.content_kind
        result["url"] = self.url

        if self.content_type is not None:
            result["content_type"] = self.content_type

        if self.content is not None:
            result["content"] = self.content

        return result

    def to_json(self):
        data = self.dump()

        content = data.get("content", None)

        if isinstance(content, bytes):
            data["content"] = base64.b64encode(content).decode("ascii")

        return json.dumps(data)


@message("ensure_task_response")
class EnsureTaskResponse(Response):
    __slots__ = (
        "task_id",
        "alias",
        "content_kind",
        "url",
        "parent_id",
    )

    def __init__(
        self,
        *args,
        task_id=None,
        alias=None,
        content_kind=None,
        url=None,
        parent_id=None,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)

        self.task_id = task_id
        self.alias = alias
        self.content_kind = content_kind
        self.url = url
        self.parent_id = parent_id

    def dump(self):
        result = super().dump()

        if self.task_id is not None:
            result["alias"] = self.alias

        if self.alias:
            result["alias"] = self.alias

        if self.content_kind:
            result["content_kind"] = self.content_kind

        if self.url:
            result["url"] = self.url

        if self.parent_id is not None:
            result["parent_id"] = self.parent_id

        return result
