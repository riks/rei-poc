import math
import random
import logging
import asyncio

import aiohttp

from . import utils
from . import messages


class AnalyserHost:
    def __init__(self, cls, url, *args, concurrency=1, loop=None, **kwargs):
        self._log = logging.getLogger(self.__class__.__name__)

        self._loop = loop

        self._cls = cls
        self._url = url

        try:
            self._concurrency = max(int(concurrency), 1)
        except ValueError:
            self._concurrency = 1

        request_concurrency = math.ceil(math.sqrt(concurrency))

        self._semaphore = asyncio.Semaphore(value=request_concurrency)

        self._args = args
        self._kwargs = kwargs

        self._websocket = None
        self._futures = {}

    @property
    def semaphore(self):
        return self._semaphore

    @property
    def websocket(self):
        return self._websocket

    async def run(self):
        try:
            async with aiohttp.ClientSession(raise_for_status=True) as session:
                async with session.ws_connect(self._url) as websocket:
                    self._websocket = websocket

                    self._analysers = [
                        self._cls(
                            self,
                            index,
                            *self._args,
                            loop=self._loop,
                            **self._kwargs,
                        )
                        for index in range(self._concurrency)
                    ]

                    await asyncio.gather(*self._analysers, loop=self._loop)

                    await self._run()
        except Exception:
            logging.exception("Analyser error")
        finally:
            self._websocket = None

            for future in self._futures.values():
                if not future.done():
                    future.cancel()

    def send_message(self, message):
        self._log.debug("%r", message)

        if self._websocket:
            self._websocket.send_str(message.to_json())

    async def request(self, message):
        while message.id in self._futures:
            message.id = None

        future = asyncio.Future(loop=self._loop)

        self._futures[message.id] = future

        self.send_message(message)

        try:
            return await asyncio.wait_for(future, 150)
        finally:
            if self._futures.get(message.id, None) is future:
                del self._futures[message.id]

            if not future.done():
                future.cancel()

    async def _run(self):
        try:
            while not self._websocket.closed:
                data = await self._websocket.receive()

                if data.type != aiohttp.WSMsgType.TEXT:
                    continue

                message = messages.Message.from_json(data.data)

                self._log.debug("%r", message)

                if message.id in self._futures:
                    future = self._futures[message.id]

                    del self._futures[message.id]

                    if not future.done():
                        future.set_result(message)

                        continue

                if message.destination is None:
                    self._log.warn(
                        "Got message without destination: %r",
                        message,
                    )

                    continue

                for analyser in self._analysers:
                    if isinstance(analyser, BaseAnalyser) and\
                       analyser.id == message.destination:
                        break
                else:
                    self._log.warn(
                        "Got message with unknown destination: %r",
                        message.destination,
                    )

                    continue

                handler_name = "receive_" + message.type

                handler = None

                try:
                    handler = object.__getattribute__(analyser, handler_name)
                except AttributeError:
                    self._log.warn(
                        "%s Unknown message type: %r",
                        analyser.name,
                        message.type,
                    )

                    continue

                result = handler(message)

                if asyncio.iscoroutine(result):
                    result = await result
        finally:
            for analyser in self._analysers:
                if analyser.id and not self._websocket.closed:
                    self.send_message(messages.ByeMessage(source=analyser.id))


class BaseAnalyser(utils.AsyncInit):
    async def __ainit__(
        self,
        host,
        index,
        alias,
        client_classes=(),
        loop=None,
    ):
        self._log = logging.getLogger(self.__class__.__name__)

        self._loop = loop or asyncio.get_event_loop()

        self._host = host
        self._name = "{0}#{1}".format(alias, index)
        self._alias = alias
        self._client_classes = client_classes

        self._id = None

        self._stack = []
        self._first_fetch = True

        asyncio.ensure_future(self.request(messages.HelloRequest(
            client_type="analyser",
            alias=self._alias,
            client_classes=self._client_classes
        ))).add_done_callback(
            lambda task: self.receive_hello_response(task.result()),
        )

    @property
    def log(self):
        return self._log

    @property
    def name(self):
        return self._name

    @property
    def alias(self):
        return self._alias

    @property
    def id(self):
        return self._id

    def delay(self):
        return 0

    @classmethod
    def run(cls, *args, **kwargs):
        return AnalyserHost(cls, *args, **kwargs).run()

    def send_message(self, message):
        return self._host.send_message(message)

    def request(self, message):
        return self._host.request(message)

    def receive_analyse_request(self, message):
        asyncio.ensure_future(self._analyser_main(message), loop=self._loop)

    def receive_hello_response(self, message):
        message.raise_error()

        self._id = message.destination

        self._log.info("%s: Analyser %r connected", self._name, self._id)

    def push_task(self, task):
        if task not in self._stack:
            self._stack.append(task)

    def pop_task(self):
        return self._stack.pop()

    async def lock_task(self, task):
        locked = await self.request(messages.LockRequest(
            source=self.id,
            task_id=task.task_id,
            alias=task.alias,
        ))

        locked.raise_error()

        return locked.locked

    def unlock_task(self, task):
        self.send_message(messages.UnlockMessage(
            source=self.id,
            task_id=task.task_id,
            alias=task.alias,
        ))

    def bad_fetcher(self):
        self.send_message(messages.BadClientMessage(
            source=self.id,
        ))

        self._first_fetch = True

    async def fetch(self, task, client_classes=None, delay=None):
        if delay is None:
            delay = self.delay

        if callable(delay):
            delay = delay()

            if asyncio.iscoroutine(delay):
                delay = await delay

        if delay is not None and delay != 0 and not self._first_fetch:
            await asyncio.sleep(delay)

        self._first_fetch = False

        async with self._host.semaphore:
            result = await self.request(messages.FetchRequest(
                source=self.id,
                task_id=task.task_id,
                alias=task.alias,
                client_classes=client_classes,
                content_kind=task.content_kind,
                url=task.url,
                parent_id=task.parent_id,
            ))

            if result.status_code >= 200 and result.status_code < 300:
                self._log.info(
                    "%s: Resource %r fetched",
                    self._name,
                    result.url,
                )
            elif result.status_code >= 300 and result.status_code < 400:
                self._log.debug(
                    "%s: Resource %r loaded from database",
                    self._name,
                    result.url,
                )

                self._first_fetch = True
            elif result.status_code >= 400:
                self._log.error(
                    "%s: Resource %r failed",
                    self._name,
                    result.url,
                )

            result.raise_error()

            return result

    async def ensure_task(self, alias, content_kind, url, parent_id=None):
        result = await self.request(messages.EnsureTaskRequest(
            source=self.id,
            alias=alias,
            content_kind=content_kind,
            url=url,
            parent_id=parent_id,
        ))

        result.raise_error()

        if result.status_code != 200:
            return None

        result = messages.AnalyseRequest(
            destination=self.id,
            task_id=result.task_id,
            alias=result.alias,
            content_kind=result.content_kind,
            url=result.url,
            parent_id=result.parent_id,
        )

        self.push_task(result)

        return result

    async def _analyser_main(self, message):
        self.push_task(message)

        while len(self._stack):
            try:
                task = self.pop_task()

                handler_name = "handle_" + task.content_kind

                handler = None

                try:
                    handler = object.__getattribute__(self, handler_name)
                except AttributeError:
                    self._log.warn(
                        "%s: Unknown content kind: %r",
                        self._name,
                        task.content_kind,
                    )

                    continue

                if not await self.lock_task(task):
                    if len(self._stack):
                        continue
                    else:
                        self.send_message(messages.AnalyseResponse(
                            id=message.id,
                            source=self.id,
                            status_code=409,
                            status_text="Conflict",
                            task_id=task.task_id,
                            alias=task.alias,
                        ))

                        return

                try:
                    result = handler(task)

                    if asyncio.iscoroutine(result):
                        result = await result
                finally:
                    self.unlock_task(task)

                if len(self._stack):
                    self.send_message(messages.AnalyseResponse(
                        id=message.id,
                        source=self.id,
                        status_code=100,
                        status_text="Continue",
                        task_id=task.task_id,
                        alias=task.alias,
                    ))
                else:
                    self.send_message(messages.AnalyseResponse(
                        id=message.id,
                        source=self.id,
                        status_code=200,
                        status_text="OK",
                        task_id=task.task_id,
                        alias=task.alias,
                    ))
            except Exception:
                self._stack = []

                self.send_message(messages.AnalyseResponse(
                    id=message.id,
                    source=self.id,
                    status_code=500,
                    status_text="Internal Server Error",
                    task_id=task.task_id,
                    alias=task.alias,
                ))

                self._log.exception("%s: Analyser error", self._name)
