import logging
import asyncio

from abc import abstractmethod

import aiohttp

from . import utils
from . import messages


class FetcherHost(utils.AsyncInit):
    async def __ainit__(self, url, *, loop=None):
        self._log = logging.getLogger(self.__class__.__name__)

        self._loop = loop

        self._url = url

        self._websocket = None
        self._futures = {}
        self._fetchers = []
        self._fetcher_index = 0
        self._close_future = asyncio.Future(loop=self._loop)

        self._init_future = asyncio.Future(loop=self._loop)

        asyncio.ensure_future(self._init(), loop=self._loop)

    @property
    def next_index(self):
        index = self._fetcher_index

        self._fetcher_index += 1

        return index

    @property
    def websocket(self):
        return self._websocket

    async def wait(self):
        await self._close_future

    def add(self, fetcher):
        self._fetchers.append(fetcher)

    def remove(self, fetcher):
        if fetcher in self._fetchers:
            self._fetchers.remove(fetcher)

    async def spawn(self, cls, *args, **kwargs):
        fetcher = cls(*args, host=self, loop=self._loop)

        self.add(fetcher)

        await fetcher

        return fetcher

    def send_message(self, message):
        self._log.debug("%r", message)

        if self._websocket:
            self._websocket.send_str(message.to_json())

    async def request(self, message):
        while message.id in self._futures:
            message.id = None

        future = asyncio.Future(loop=self._loop)

        self._futures[message.id] = future

        self.send_message(message)

        try:
            return await future
        finally:
            if self._futures.get(message.id, None) is future:
                del self._futures[message.id]

            if not future.done():
                future.cancel()

    async def _init(self):
        try:
            async with aiohttp.ClientSession(raise_for_status=True) as session:
                async with session.ws_connect(self._url) as websocket:
                    self._websocket = websocket

                    self._init_future.set_result(None)

                    await self._run()
        except Exception as exception:
            logging.exception("Fetcher error")

            if not self._init_future.done() or self._init_future.cancelled():
                self._init_future.set_exception(exception)
        finally:
            self._websocket = None

            self._close_future.set_result(None)

            for future in self._futures.values():
                if not future.done():
                    future.cancel()

    async def _run(self):
        try:
            while not self._websocket.closed:
                data = await self._websocket.receive()

                if data.type != aiohttp.WSMsgType.TEXT:
                    continue

                message = messages.Message.from_json(data.data)

                self._log.debug("%r", message)

                if message.id in self._futures:
                    future = self._futures[message.id]

                    del self._futures[message.id]

                    if not future.done():
                        future.set_result(message)

                        continue

                if message.destination is None:
                    self._log.warn(
                        "Got message without destination: %r",
                        message,
                    )

                    continue

                for fetcher in self._fetchers:
                    if isinstance(fetcher, BaseFetcher) and\
                       fetcher.id == message.destination:
                        break
                else:
                    self._log.warn(
                        "Got message with unknown destination: %r",
                        message.destination,
                    )

                    continue

                handler_name = "receive_" + message.type

                handler = None

                try:
                    handler = object.__getattribute__(fetcher, handler_name)
                except AttributeError:
                    self._log.warn(
                        "%s Unknown message type: %r",
                        fetcher.name,
                        message.type,
                    )

                    continue

                result = handler(message)

                if asyncio.iscoroutine(result):
                    result = await result
        except Exception:
            logging.exception("Fetcher error")
        finally:
            for fetcher in self._fetchers:
                if fetcher.id and not self._websocket.closed:
                    self.send_message(messages.ByeMessage(source=fetcher.id))


class BaseFetcher(utils.AsyncInit):
    async def __ainit__(self, *, host=None, index=None, loop=None):
        self._log = logging.getLogger(self.__class__.__name__)

        self._loop = loop or asyncio.get_event_loop()

        self._host = host
        self._index = index if index is not None else self._host.next_index

        self._id = None
        self._alias = None
        self._client_classes = None

        asyncio.ensure_future(self._run(), loop=self._loop)

    @property
    def log(self):
        return self._log

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return str(self._index)

    @property
    def alias(self):
        return self._alias

    @property
    def client_classes(self):
        return self._client_classes

    @property
    def websocket(self):
        return self._host.websocket

    def send_message(self, message):
        return self._host.send_message(message)

    def request(self, message):
        return self._host.request(message)

    async def begin(self, alias=None, client_classes=None):
        self._alias = alias
        self._client_classes = client_classes

        result = await self.request(messages.HelloRequest(
            client_type="fetcher",
            alias=alias,
            client_classes=client_classes,
        ))

        self._id = result.destination

        self._log.info("%s: Fetcher %r connected", self.name, self._id)

    def end(self):
        if self._id is None:
            return

        message = messages.ByeMessage(source=self._id)

        self._id = None

        self._host.send_message(message)

        self._log.info(
            "%s: Fetcher %r disconnected",
            self.name,
            message.source,
        )

    async def bad_client(self, message):
        self.end()

    @abstractmethod
    async def main(self):
        pass

    @abstractmethod
    async def fetch(self, message):
        pass

    async def receive_fetch_request(self, message):
        asyncio.ensure_future(self.fetch(message))

    async def receive_bad_client_message(self, message):
        asyncio.ensure_future(self.bad_client(message), loop=self._loop)

    async def _run(self):
        await self.main()

        if self._id:
            self.end()

        self._host.remove(self)
