from abc import abstractmethod, ABCMeta
from asyncio import iscoroutine


class AsyncInit(metaclass=ABCMeta):
    def __init__(self, *args, **kwargs):
        async def _ainit():
            result = self.__ainit__(*args, **kwargs)

            if iscoroutine(result):
                result = await result

            del self._ainit

            return self

        self._ainit = _ainit

    def __await__(self):
        return self._ainit().__await__()

    @abstractmethod
    def __ainit__(self):
        pass


class AsyncContext(AsyncInit):
    def __aenter__(self):
        pass
