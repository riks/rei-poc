import re
import time
import asyncio
import logging

import aiohttp
from aiohttp.client_exceptions import certificate_errors, ssl_errors

from aiosocks import Socks5Addr
from aiosocks import SocksError, SocksConnectionError, InvalidServerVersion
from aiosocks.connector import ProxyConnector
from aiosocks.protocols import Socks5Protocol, DEFAULT_LIMIT
from aiosocks.constants import SOCKS_VER5, SOCKS5_GRANTED, SOCKS5_ERRORS, RSV

from . import utils


_CODE_REGEX = re.compile(r"^([0-9]{3})([ -+])(.*)$")

_CONF_REGEX = re.compile(r"\s*(\w+)=([^\s]+)")

_EXTEND_REGEX = re.compile(r"^(\w+)\s+([0-9]+)$")

_EVENT_REGEX = re.compile(r"^(\w+)\s+(.*)$")

_CIRC_REGEX = re.compile(r"^([0-9]+)\s+(\w+)\s+([^\s]+)(.*)$")
_CIRC_DATA_REGEX = re.compile(r"\s+(\w+)=([^\s]+)")

_STREAM_REGEX = re.compile(r"^([0-9]+)\s+(\w+)\s+([0-9]+)\s+([^\s]+)(.*)$")
_STREAM_DATA_REGEX = re.compile(r"\s+(\w+)=([^\s]+)")


class TorError(Exception):
    def __init__(self, code, message, *args, **kwargs):
        msg = message

        if code is not None:
            msg += " ({0})".format(code)

        super().__init__(msg, *args, **kwargs)


class TorSocksProtocol(Socks5Protocol):
    def __init__(self, control, circuit, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._control = control
        self._circuit = circuit

    def connection_made(self, transport):
        self._address = transport.get_extra_info("sockname")

        super().connection_made(transport)

    async def socks_request(self, cmd):
        await self.authenticate()

        # build and send command
        dst_addr, resolved = await self.build_dst_address(
            self._dst_host,
            self._dst_port,
        )

        self.write_request([SOCKS_VER5, cmd, RSV] + dst_addr)

        self._stream = await self._control.resolve_stream(
            "{0}:{1}".format(self._address[0], self._address[1])
        )

        await self._control.attach_stream(self._stream, self._circuit)

        # read/process command response
        resp = await self.read_response(3)

        if resp[0] != SOCKS_VER5:
            raise InvalidServerVersion(
                "SOCKS5 proxy server sent invalid version"
            )

        if resp[1] != SOCKS5_GRANTED:
            error = SOCKS5_ERRORS.get(resp[1], "Unknown error")

            raise SocksError("[Errno {0:#04x}]: {1}".format(resp[1], error))

        binded = await self.read_address()

        return resolved, binded


class TorConnector(ProxyConnector):
    def __init__(self, proxy, control, circuit, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._proxy = proxy
        self._control = control
        self._circuit = circuit

    @property
    def control(self):
        return self._control

    async def close_circuit(self):
        await self._control.close_circuit(self._circuit)

    async def _create_connection(self, req):
        sslcontext = self._get_ssl_context(req)
        fingerprint, hashfunc = self._get_fingerprint_and_hashfunc(req)

        if not self._remote_resolve:
            try:
                dst_hosts = list(await self._resolve_host(req.host, req.port))
                dst = dst_hosts[0]["host"], dst_hosts[0]["port"]
            except OSError as exc:
                raise aiohttp.ClientConnectorError(
                    req.connection_key,
                    exc,
                ) from exc
        else:
            dst = req.host, req.port

        last_exc = None

        self._proxy_hosts = await self._resolve_host(
            self._proxy.host,
            self._proxy.port,
        )

        for hinfo in self._proxy_hosts:
            try:
                transport, proto = await self._wrap_create_socks_connection(
                    self._factory,
                    dst,
                    loop=self._loop,
                    remote_resolve=self._remote_resolve,
                    ssl=sslcontext,
                    family=hinfo["family"],
                    proto=hinfo["proto"],
                    flags=hinfo["flags"],
                    local_addr=self._local_addr,
                    req=req,
                    server_hostname=req.host if sslcontext else None,
                )
            except aiohttp.ClientConnectorError as exc:
                last_exc = exc

                continue

            has_cert = transport.get_extra_info("sslcontext")

            if has_cert and fingerprint:
                sock = transport.get_extra_info("socket")

                if not hasattr(sock, "getpeercert"):
                    # Workaround for asyncio 3.5.0
                    # Starting from 3.5.1 version
                    # there is 'ssl_object' extra info in transport
                    sock = transport._ssl_protocol._sslpipe.ssl_object

                # gives DER-encoded cert as a sequence of bytes (or None)
                cert = sock.getpeercert(binary_form=True)

                assert cert

                got = hashfunc(cert).digest()
                expected = fingerprint

                if got != expected:
                    transport.close()

                    if not self._cleanup_closed_disabled:
                        self._cleanup_closed_transports.append(transport)

                    last_exc = aiohttp.ServerFingerprintMismatch(
                        expected, got, req.host, req.port)

                    continue

            return proto
        else:
            raise last_exc

    async def _create_tor_connection(
        self,
        protocol_factory,
        dst,
        *,
        remote_resolve=True,
        loop=None,
        ssl=None,
        family=0,
        proto=0,
        flags=0,
        sock=None,
        local_addr=None,
        server_hostname=None,
        reader_limit=DEFAULT_LIMIT,
    ):
        loop = loop or asyncio.get_event_loop()
        waiter = asyncio.Future(loop=loop)

        def socks_factory():
            return TorSocksProtocol(
                self._control,
                self._circuit,
                proxy=self._proxy,
                proxy_auth=None,
                dst=dst,
                app_protocol_factory=protocol_factory,
                waiter=waiter,
                remote_resolve=remote_resolve,
                loop=loop,
                ssl=ssl,
                server_hostname=server_hostname,
                reader_limit=reader_limit
            )

        try:
            transport, protocol = await loop.create_connection(
                socks_factory,
                self._proxy.host,
                self._proxy.port,
                family=family,
                proto=proto,
                flags=flags,
                sock=sock,
                local_addr=local_addr,
            )
        except OSError as exc:
            raise SocksConnectionError(
                "[Errno %s] Can not connect to proxy %s:%d [%s]" %
                (exc.errno, self._proxy.host, self._proxy.port, exc.strerror)
            ) from exc

        try:
            await waiter
        except Exception:
            transport.close()

            raise

        return protocol.app_transport, protocol.app_protocol

    async def _wrap_create_socks_connection(self, *args, req, **kwargs):
        try:
            return await self._create_tor_connection(*args, **kwargs)
        except certificate_errors as exc:
            raise aiohttp.ClientConnectorCertificateError(
                req.connection_key, exc) from exc
        except ssl_errors as exc:
            raise aiohttp.ClientConnectorSSLError(
                req.connection_key, exc) from exc
        except (OSError, SocksConnectionError) as exc:
            raise aiohttp.ClientProxyConnectionError(
                req.connection_key,
                exc,
            ) from exc


class TorControl(utils.AsyncInit):
    async def __ainit__(self, socks_port=None, control_port=None, loop=None):
        self._log = logging.getLogger(__name__)

        self._loop = loop or asyncio.get_event_loop()
        self._lock = asyncio.Lock(loop=self._loop)

        self._socks = socks_port
        self._control = control_port

        self._reader, self._writer = await asyncio.open_connection(
            self._control[0],
            self._control[1],
            loop=self._loop,
        )

        self._executes = []
        self._resolves = {}

        self._reader_task = asyncio.ensure_future(self._reader_main())

        self._clients = {}
        self._circuits = {}
        self._streams = {}

        self._blacklist = {}

        await self._execute("AUTHENTICATE")
        await self._execute("SETCONF NewCircuitPeriod=2592000")
        await self._execute("SETCONF MaxCircuitDirtiness=2592000")
        await self._execute("SETCONF CircuitIdleTimeout=2592000")
        await self._execute("SETCONF CircuitBuildTimeout=15")
        await self._execute("SETCONF CircuitStreamTimeout=30")
        await self._execute("SETCONF MaxClientCircuitsPending=1024")
        await self._execute("SETCONF __DisablePredictedCircuits=1")
        await self._execute("SETCONF __LeaveStreamsUnattached=1")
        await self._execute("SETCONF ExcludeExitNodes=")
        await self._execute("SETEVENTS EXTENDED CIRC STREAM")

    @property
    def proxy(self):
        return Socks5Addr(self._socks[0], self._socks[1])

    async def connector(self, *args, **kwargs):
        circuit = await self.create_circuit()

        if not circuit:
            return None

        return TorConnector(self.proxy, self, circuit, *args, **kwargs)

    async def apply_blacklist(self):
        changed = False

        for node, timestamp in list(self._blacklist.items()):
            if not timestamp:
                continue

            if timestamp < time.time() and node in self._blacklist:
                del self._blacklist[node]

                changed = True

        if changed:
            await self._execute(
                "SETCONF ExcludeExitNodes=" + ",".join(self._blacklist.keys()),
            )

    async def create_circuit(self):
        await self.apply_blacklist()

        for _ in range(15):
            circuit = None

            try:
                result = await self._execute("EXTENDCIRCUIT 0 PURPOSE=GENERAL")

                match = _EXTEND_REGEX.match(result)

                future = asyncio.Future(loop=self._loop)

                circuit = {
                    "id": int(match[2]),
                    "future": future
                }

                self._circuits[circuit["id"]] = circuit

                try:
                    await asyncio.wait_for(future, 90)

                    self._log.debug("Circuit %s created", circuit["id"])
                finally:
                    if not future.done():
                        future.cancel()

                    del circuit["future"]

                exit_node = circuit["path"][-1]

                if exit_node in self._blacklist:
                    await self.close_circuit(circuit["id"])

                    await asyncio.sleep(15, loop=self._loop)

                    continue

                self._blacklist[exit_node] = None

                return circuit["id"]
            except Exception:
                if circuit is not None:
                    await self.close_circuit(circuit["id"])

                    circuit = None

        return None

    async def close_circuit(self, circuit):
        try:
            await self._execute("CLOSECIRCUIT {0}".format(circuit))

            if circuit in list(self._circuits):
                path = self._circuits[circuit].get("path", None)

                if path:
                    exit_node = path[-1]

                    if exit_node in self._blacklist and\
                       not self._blacklist[exit_node]:
                        del self._blacklist[exit_node]

                del self._circuits[circuit]
        except TorError:
            pass

        self._log.debug("Circuit %s closed", circuit)

    async def attach_stream(self, stream, circuit):
        future = self._streams[stream].get("future", None) or \
            asyncio.Future(loop=self._loop)

        self._streams[stream]["future"] = future

        await self._execute("ATTACHSTREAM {0} {1}".format(stream, circuit))

        self._log.debug("Stream %s attached to circuit %s", stream, circuit)

        try:
            await asyncio.wait_for(future, 90)
        finally:
            if not future.done():
                future.cancel()

            if stream in self._streams:
                del self._streams[stream]["future"]

    async def resolve_stream(self, client):
        if client in self._clients:
            stream = self._clients[client]["stream"]

            if stream in self._streams:
                del self._clients[client]

                return self._streams[stream]["id"]

        future = asyncio.Future(loop=self._loop)

        self._resolves[client] = future

        try:
            return await asyncio.wait_for(future, 90)
        finally:
            del self._resolves[client]

    async def close(self):
        for circuit in self._circuits:
            await self.close_circuit(circuit)

        self._writer.write("QUIT\r\n".encode("ascii"))

        await self._writer.drain()

        self._writer.close()

        await self._reader_task

    async def _execute(self, command):
        self._log.debug(command)

        future = asyncio.Future(loop=self._loop)

        async with self._lock:
            self._writer.write((command + "\r\n").encode("ascii"))

            self._executes.insert(0, future)

            await self._writer.drain()

        code, result = await future

        if code >= 300:
            raise TorError(code, result)

        return result

    async def _reader_main(self):
        code = None
        modifier = None
        message = ""
        reply = None

        while not self._reader.at_eof():
            data = (await self._reader.readline()).decode("ascii").strip()

            if not data:
                continue

            self._log.debug(data)

            match = _CODE_REGEX.match(data)

            if match is None:
                if data == ".":
                    reply = (code, message)
                else:
                    message += "\n" + data
            else:
                code = int(match[1])
                modifier = match[2]
                message += match[3]

                if modifier == " ":
                    reply = (code, message)
                elif modifier == "-":
                    message += " "

            if reply is not None:
                if code < 600:
                    future = self._executes.pop()

                    future.set_result(reply)
                else:
                    try:
                        await self._got_event(code, message)
                    except Exception:
                        self._log.exception("Event handling error")

                        raise

                code = None
                modifier = None
                message = ""
                reply = None

    async def _got_event(self, code, message):
        match = _EVENT_REGEX.match(message)

        event = match[1].upper()
        data = match[2]

        if event == "CIRC":
            match = _CIRC_REGEX.match(data)

            data = {
                "id": int(match[1]),
                "status": match[2],
                "path": match[3].split(","),
                "params": dict([
                    (item[0].lower(), item[1].upper()) for item in
                    _CIRC_DATA_REGEX.findall(match[4])
                ]),
            }

            self._got_circuit_event(data)
        elif event == "STREAM":
            match = _STREAM_REGEX.match(data)

            data = {
                "id": int(match[1]),
                "status": match[2],
                "circuit": int(match[3]),
                "target": match[4],
                "params": dict([
                    (item[0].lower(), item[1].upper()) for item in
                    _STREAM_DATA_REGEX.findall(match[5])
                ]),
            }

            self._got_stream_event(data)

    def _got_circuit_event(self, event):
        if event["id"] in self._circuits:
            if event["status"] == "BUILT":
                self._circuits[event["id"]].update(event)

                future = self._circuits[event["id"]].get("future", None)

                if future and not future.done():
                    future.set_result(None)
            elif event["status"] == "CLOSED":
                future = self._circuits[event["id"]].get("future", None)

                del self._circuits[event["id"]]

                if future and not future.done():
                    future.set_result(None)
            elif event["status"] == "FAILED":
                future = self._circuits[event["id"]].get("future", None)

                del self._circuits[event["id"]]

                if future and not future.done():
                    future.set_exception(TorError(
                        " ".join([
                            item[0] + "=" + item[1]
                            for item in event["params"].items()
                        ]),
                        "Circuit failed")
                    )
            else:
                self._circuits[event["id"]].update(event)

    def _got_stream_event(self, event):
        if event["status"] == "NEW":
            self._streams[event["id"]] = event

            source_addr = event["params"].get("source_addr", None)

            self._clients[source_addr] = {
                "stream": event["id"],
            }

            if source_addr in self._resolves:
                self._resolves[source_addr].set_result(event["id"])
        elif event["status"] == "CLOSED":
            stream = self._streams.get(event["id"], None)

            if stream:
                del self._streams[event["id"]]

                source_addr = stream["params"].get("source_addr", None)

                if source_addr in self._clients:
                    del self._clients[source_addr]

                future = stream.get("future", None)

                if future and not future.done():
                    future.cancel()
        elif event["circuit"] in self._circuits:
            self._streams[event["id"]].update(event)

            future = self._streams[event["id"]].get("future", None)

            if future and not future.done():
                if event["status"] == "SUCCEEDED":
                    future.set_result(None)
                elif event["status"] == "DETACHED":
                    stream = self._streams.get(event["id"], None)

                    source_addr = stream["params"].get("source_addr", None)

                    if source_addr in self._clients:
                        del self._clients[source_addr]

                    expire = None

                    remote_reason = event["params"].get("remote_reason")
                    reason = event["params"].get("reason")

                    if remote_reason == "EXITPOLICY":
                        expire = time.time() + 12 * 3600
                    elif remote_reason == "RESOURCELIMIT":
                        expire = time.time() + 3 * 3600
                    elif reason == "TIMEOUT":
                        expire = time.time() + 3600

                    if expire:
                        circuit = self._circuits[event["circuit"]]

                        exit_node = circuit["path"][-1]

                        self._blacklist[exit_node] = expire

                        asyncio.ensure_future(
                            self.apply_blacklist(),
                            loop=self._loop,
                        )

                    future.set_exception(TorError(
                        " ".join([
                            item[0] + "=" + item[1]
                            for item in event["params"].items()
                        ]),
                        "Stream detached"),
                    )
        elif event["circuit"] != 0:
            stream = self._streams.get(event["id"], None)

            if stream:
                del self._streams[event["id"]]

                source_addr = stream["params"].get("source_addr", None)

                if source_addr in self._clients:
                    del self._clients[source_addr]
