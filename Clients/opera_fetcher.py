import io
import re
import base64
import logging
import asyncio

from concurrent import futures

import pycurl

from rei import messages, FetcherHost, BaseFetcher


class OperaRequestor:
    STATUS_LINE_REGEXP = re.compile(r"HTTP\/\S*\s*\d+\s*(.*?)\s*$")

    EXECUTOR = futures.ThreadPoolExecutor(max_workers=750)

    def __init__(self, proxy, loop=None):
        self._log = logging.getLogger(self.__class__.__name__)

        self._loop = loop or asyncio.get_event_loop()
        self._lock = asyncio.Lock(loop=self._loop)
        self._proxy = proxy

        self._share = pycurl.CurlShare()

        self._curl = pycurl.Curl()

        self._share.setopt(pycurl.SH_SHARE, pycurl.LOCK_DATA_DNS)
        self._share.setopt(pycurl.SH_SHARE, pycurl.LOCK_DATA_COOKIE)
        self._share.setopt(pycurl.SH_SHARE, pycurl.LOCK_DATA_SSL_SESSION)

        self._curl.setopt(pycurl.SHARE, self._share)
        self._curl.setopt(pycurl.FOLLOWLOCATION, True)

        self._curl.setopt(
            pycurl.USERAGENT,
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 " +
            "(KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36 " +
            "OPR/52.0.2871.30",
        )

        self._curl.setopt(pycurl.SSL_VERIFYPEER, 0)
        self._curl.setopt(pycurl.SSL_VERIFYHOST, 0)
        self._curl.setopt(pycurl.PROXY, self._proxy)
        self._curl.setopt(pycurl.PROXYAUTH, pycurl.HTTPAUTH_BASIC)
        self._curl.setopt(pycurl.HTTPPROXYTUNNEL, 1)

        self._curl.setopt(248, 0)
        self._curl.setopt(249, 0)

    async def request(self, url):
        async with self._lock:
            return await self._loop.run_in_executor(
                self.EXECUTOR,
                self._request,
                url,
            )

    def _request(self, url):
        headers = io.BytesIO()
        data = io.BytesIO()

        self._curl.setopt(pycurl.URL, url)

        self._curl.setopt(pycurl.HEADERFUNCTION, headers.write)
        self._curl.setopt(pycurl.WRITEFUNCTION, data.write)

        self._curl.perform()

        status_text = None

        for header in headers.getvalue().decode("utf-8").splitlines():
            match = self.STATUS_LINE_REGEXP.match(header)

            if match is not None:
                status_text = match[1]

        content_type = self._curl.getinfo(self._curl.CONTENT_TYPE)

        if content_type is not None:
            content_type = content_type.split(";")[0].strip()

        if content_type is None:
            content_type = "application/octet-stream"

        result = {
            "url": self._curl.getinfo(self._curl.EFFECTIVE_URL),
            "status_code": self._curl.getinfo(self._curl.RESPONSE_CODE),
            "status_text": status_text,
            "content_type": content_type,
            "content": data.getvalue(),
        }

        return result

    def __del__(self):
        self._curl.close()
        self._share.close()


class OperaFetcher(BaseFetcher):
    async def __ainit__(self, opera, *args, index=None, loop=None, **kwargs):
        self._loop = loop or asyncio.get_event_loop()

        self._opera = opera

        self._requestor = OperaRequestor(opera, loop=self._loop)

        self._active = False

        await super().__ainit__(*args, index=index, loop=self._loop, **kwargs)

        self._name = "#{0}".format(super().name)

    @property
    def name(self):
        if self.alias:
            return "{0}#{1}".format(self.alias, super().name)
        else:
            return "#{0}".format(super().name)

    async def bad_client(self, message):
        await super().bad_client(message)

        await asyncio.sleep(3600)

        self._active = False

    async def fetch(self, message):
        try:
            result = await self._requestor.request(
                message.url,
            )

            if result["status_code"] < 300:
                self._log.info(
                    "%s: Resource %r fetched",
                    self.name,
                    result["url"],
                )
            else:
                self._log.error(
                    "%s: Resource %r failed",
                    self.name,
                    result["url"],
                )

            response = messages.FetchResponse(
                id=message.id,
                source=self.id,
                destination=message.source,
                alias=message.alias,
                task_id=message.task_id,
                content_kind=message.content_kind,
                **result,
            )

            self.send_message(response)
        except Exception as exception:
            self._log.exception("%s: Fetcher error", self.name)

            response = messages.FetchResponse(
                id=message.id,
                source=self.id,
                destination=message.source,
                status_code=500,
                status_text="Internal Server Error",
                alias=message.alias,
                task_id=message.task_id,
                content_kind=message.content_kind,
                url=message.url,
            )

            if isinstance(exception, pycurl.error):
                response.status_code = 523
                response.status_text = "Origin Is Unreachable"

            self.send_message(response)

            if isinstance(exception, pycurl.error):
                self.end()

                self._active = False

    async def main(self):
        while self.websocket is not None and not self.websocket.closed:
            if self._id is None and not self._active:
                try:
                    await self._requestor.request(
                        "http://checkip.amazonaws.com/",
                    )
                except Exception:
                    await asyncio.sleep(1800)

                    continue

                await self.begin()

                self._active = True

            await asyncio.sleep(15)


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        datefmt="%Y/%m/%d %H:%M:%S",
        format="%(asctime)s [%(levelname)s]: %(message)s",
    )

    loop = asyncio.get_event_loop()

    credentials = "MDY5OUNDNUU4QTJBNkFBQzk1ODcxNDUwNDExRDM4QkZEQTUwNDJENTpleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKcFlYUWlPakUxTWpJM01URXpOalVzSW1sa0lqb2lNRFk1T1VORE5VVTRRVEpCTmtGQlF6azFPRGNpTENKcGNDSTZJamswTGpjMkxqRXdNaTQwT0NKOS5XLV9RY2F6UDBUcHJubjFXWUF4WEtyei04YVlULTNpOGVKT1VCa0d6OU5N"

    host = loop.run_until_complete(
      FetcherHost("http://localhost:8000/api", loop=loop),
    )

    for octet3 in (244, 245, 246):
        for octet4 in range(1, 255):
            opera = "https://{0}@77.111.{1}.{2}:{3}".format(
                base64.b64decode(credentials).decode("ascii"),
                octet3,
                octet4,
                8000,
            )

            loop.run_until_complete(host.spawn(OperaFetcher, opera))

    loop.run_until_complete(host.wait())
