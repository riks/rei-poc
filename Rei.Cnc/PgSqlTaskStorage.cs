﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

using Npgsql;

using Rei.Common;
using Rei.Messages;

namespace Rei.Cnc
{
    public class PgSqlTaskStorage : ITaskStorage, IDisposable
    {
        Lock connectionLock = new Lock();
        NpgsqlConnection connection;

        HashSet<long> locks = new HashSet<long>();

        ISourceStorage sourceStorage;

        protected ILogger Logger
        {
            get;
            private set;
        }

        public PgSqlTaskStorage(IConfiguration configuration, ILoggerFactory loggerFactory, ISourceStorage sourceStorage)
        {
            this.Logger = loggerFactory.CreateLogger(this.GetType());

            this.sourceStorage = sourceStorage;

            NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder(configuration["PostgreSQL"])
            {
                Pooling = false,
                ApplicationName = "REI C'n'C Task Storage",
            };

            this.connection = new NpgsqlConnection(builder.ToString());
        }

        ~PgSqlTaskStorage()
        {
            this.Dispose();
        }

        public async Task<EnsureTaskResponse> EnqueueTask(EnsureTaskRequest request)
        {
            if (!await this.sourceStorage.SourceExpiredAsync(request))
            {
                return new EnsureTaskResponse
                {
                    Id = request.Id,
                    Destination = request.Source,
                    StatusCode = 204,
                };
            }

            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"INSERT INTO ""rdw"".""task"" (
    ""alias"",
    ""content_kind"",
    ""url"",
    ""parent""
) VALUES (
    @alias,
    @content_kind,
    @url,
    @parent
) ON CONFLICT DO NOTHING
RETURNING
    ""id"",
    ""alias"",
    ""content_kind"",
    ""url"",
    ""parent"";";

                    command.Parameters.AddWithValue("alias", request.Alias);
                    command.Parameters.AddWithValue("content_kind", request.ContentKind);
                    command.Parameters.AddWithValue("url", request.Url);

                    if (request.ParentId.HasValue)
                    {
                        command.Parameters.AddWithValue("parent", request.ParentId);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("parent", DBNull.Value);
                    }

                    using (DbDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (!await reader.ReadAsync())
                        {
                            return new EnsureTaskResponse
                            {
                                Id = request.Id,
                                Destination = request.Source,
                                StatusCode = 204,
                            };
                        }

                        long taskId = (long)reader["id"];
                        string alias = (string)reader["alias"];
                        string contentKind = (string)reader["content_kind"];
                        string url = (string)reader["url"];
                        long? parentId = reader["parent"] as long?;

                        this.Logger.LogDebug("Task {0} enqueud", taskId);

                        return new EnsureTaskResponse
                        {
                            Id = request.Id,
                            Destination = request.Source,
                            StatusCode = 200,
                            TaskId = taskId,
                            Alias = alias,
                            ContentKind = contentKind,
                            Url = url,
                            ParentId = parentId,
                        };
                    }
                }
            }
        }

        public async Task<AnalyseRequest> DequeuTaskAsync(string alias)
        {
            _ = alias ?? throw new ArgumentNullException(nameof(alias));

            AnalyseRequest result = null;

            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }


                using (NpgsqlCommand command = this.connection.CreateCommand())
                {

                    command.CommandText = @"WITH ""w"" AS (
    SELECT
        ""id"",
        ""content_kind"",
        ""url"",
        ""parent""
    FROM ""rdw"".""task""
    WHERE ""alias"" = @alias AND ""parent"" IS NULL
    LIMIT 256
) SELECT
    *
FROM ""w""
ORDER BY ""random""();";

                    command.Parameters.AddWithValue("alias", alias);

                    using (DbDataReader reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            long taskId = (long)reader["id"];

                            int lockKey = Crc32.Calculate(alias);
                            int lockId = (int)taskId;
                            long lockHash = (long)lockKey << 32 + lockId;

                            if (this.locks.Contains(lockHash))
                            {
                                continue;
                            }

                            string contentKind = (string)reader["content_kind"];
                            string url = (string)reader["url"];
                            long? parentId = reader["parent"] as long?;

                            this.Logger.LogDebug("Task {0} dequeued", taskId);

                            result = new AnalyseRequest
                            {
                                TaskId = taskId,
                                Alias = alias,
                                ContentKind = contentKind,
                                Url = url,
                                ParentId = parentId,
                            };

                            break;
                        }
                    }
                }
            }

            return result;
        }

        public async Task CompleteTaskAsync(AnalyseResponse response)
        {
            if (response.StatusCode != 100 && response.StatusCode != 200)
            {
                return;
            }

            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"DELETE FROM ""rdw"".""task"" WHERE ""id"" = @task_id;";

                    command.Parameters.AddWithValue("task_id", response.TaskId);

                    await command.ExecuteNonQueryAsync();

                    this.Logger.LogDebug("Task {0} complete", response.TaskId);
                }
            }
        }

        public async Task<LockResponse> LockTaskAsync(LockRequest request)
        {
            int lockKey = Crc32.Calculate(request.Alias);
            int lockId = (int)request.TaskId;
            long lockHash = (long)lockKey << 32 + lockId;

            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }

                if (this.locks.Contains(lockHash))
                {
                    return new LockResponse
                    {
                        Id = request.Id,
                        Destination = request.Source,
                        StatusCode = (ushort)StatusCode.OK,
                        TaskId = request.TaskId,
                        Alias = request.Alias,
                        Locked = false,
                    };
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"WITH ""w"" AS (
    SELECT
        ""id""
    FROM ""rdw"".""task""
    WHERE ""id"" = @id
) SELECT
    ""pg_try_advisory_lock""(@classid, @objid) ""locked""
FROM ""w"";";

                    command.Parameters.AddWithValue("id", request.TaskId);
                    command.Parameters.AddWithValue("classid", lockKey);
                    command.Parameters.AddWithValue("objid", lockId);

                    bool locked = (bool?)await command.ExecuteScalarAsync() ?? false;

                    if (locked)
                    {
                        this.Logger.LogDebug("Task {0} locked", request.TaskId);

                        this.locks.Add(lockHash);
                    }
                    else
                    {
                        this.Logger.LogDebug("Task {0} not locked", request.TaskId);
                    }

                    return new LockResponse
                    {
                        Id = request.Id,
                        Destination = request.Source,
                        StatusCode = (ushort)StatusCode.OK,
                        TaskId = request.TaskId,
                        Alias = request.Alias,
                        Locked = locked,
                    }; ;
                }
            }
        }

        public async Task UnlockTaskAsync(string alias, long taskId)
        {
            int lockKey = Crc32.Calculate(alias);
            int lockId = (int)taskId;
            long lockHash = (long)lockKey << 32 + lockId;

            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }

                if (!this.locks.Contains(lockHash))
                {
                    return;
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"SELECT ""pg_advisory_unlock""(@classid, @objid);";

                    command.Parameters.AddWithValue("classid", lockKey);
                    command.Parameters.AddWithValue("objid", lockId);

                    await command.ExecuteScalarAsync();
                }

                this.locks.Remove(lockHash);

                this.Logger.LogDebug("Task {0} unlocked", taskId);
            }
        }

        public void Dispose()
        {
            if (this.connection.State != ConnectionState.Closed)
            {
                this.connection.Close();
            }

            GC.SuppressFinalize(this);
        }
    }
}
