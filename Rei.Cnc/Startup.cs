﻿using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Serilog;

using Npgsql.Logging;

namespace Rei.Cnc
{
    public class Startup
    {
        public IConfiguration Configuration
        {
            get;
        }

        public Startup(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(builder => builder.ClearProviders().AddSerilog());

            services.AddSingleton<INpgsqlLoggingProvider, NpgsqlLoggingProvider>();
            services.AddSingleton<ITaskStorage, PgSqlTaskStorage>();
            services.AddSingleton<ISourceStorage, PgSqlSourceStorage>();
            services.AddSingleton<Server>();

            services.AddScoped<WebSocketClient>();

            services.AddTransient<Analyser>();
            services.AddTransient<Fetcher>();

            services.AddRouting();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            NpgsqlLogManager.Provider = app.ApplicationServices.GetService<INpgsqlLoggingProvider>();

            if (env.IsDevelopment())
            {
                NpgsqlLogManager.IsParameterLoggingEnabled = true;
            }

            Server server = app.ApplicationServices.GetService<Server>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler(new ExceptionHandlerOptions()
                {
                    ExceptionHandler = server.ErrorHandler,
                });
            }

            app.UseStatusCodePagesWithReExecute("/error");

            app.UseDefaultFiles(new DefaultFilesOptions()
            {
                DefaultFileNames = new string[] { "index.htm", "index.html" },
            });

            app.UseStaticFiles();

            app.UseWebSockets();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            app.UseRouter(routeBuilder =>
            {
                routeBuilder.MapGet("api", server.WebSocketApi);
                routeBuilder.MapGet("error", server.ErrorHandler);
            });
        }
    }
}
