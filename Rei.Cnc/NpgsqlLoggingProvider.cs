﻿using Npgsql.Logging;

using Microsoft.Extensions.Logging;
using System;

namespace Rei.Cnc
{
    public class NpgsqlLoggingProvider : INpgsqlLoggingProvider
    {
        ILoggerFactory loggerFactory;

        class Logger : NpgsqlLogger
        {
            ILogger logger;

            public Logger(ILoggerFactory loggerFactory, string name)
            {
                this.logger = loggerFactory.CreateLogger(name);
            }

            public override bool IsEnabled(NpgsqlLogLevel level)
            {
                switch (level)
                {
                    case NpgsqlLogLevel.Trace:
                        return this.logger.IsEnabled(LogLevel.Trace);
                    case NpgsqlLogLevel.Debug:
                        return this.logger.IsEnabled(LogLevel.Debug);
                    case NpgsqlLogLevel.Info:
                        return this.logger.IsEnabled(LogLevel.Information);
                    case NpgsqlLogLevel.Warn:
                        return this.logger.IsEnabled(LogLevel.Warning);
                    case NpgsqlLogLevel.Error:
                        return this.logger.IsEnabled(LogLevel.Error);
                    case NpgsqlLogLevel.Fatal:
                        return this.logger.IsEnabled(LogLevel.Critical);
                    default:
                        return false;
                }
            }

            public override void Log(NpgsqlLogLevel level, int connectorId, string message, Exception exception = null)
            {
                switch (level)
                {
                    case NpgsqlLogLevel.Trace:
                        this.logger.LogTrace(exception, message);

                        break;
                    case NpgsqlLogLevel.Debug:
                        this.logger.LogDebug(exception, message);

                        break;
                    case NpgsqlLogLevel.Info:
                        this.logger.LogInformation(exception, message);

                        break;
                    case NpgsqlLogLevel.Warn:
                        this.logger.LogWarning(exception, message);

                        break;
                    case NpgsqlLogLevel.Error:
                        this.logger.LogError(exception, message);

                        break;
                    case NpgsqlLogLevel.Fatal:
                        this.logger.LogCritical(exception, message);
                        break;

                }
            }
        }

        public NpgsqlLoggingProvider(ILoggerFactory loggerFactory)
        {
            this.loggerFactory = loggerFactory;
        }

        public NpgsqlLogger CreateLogger(string name) => new Logger(this.loggerFactory, name);
    }
}
