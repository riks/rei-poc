﻿using System;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;

using Rei.Common;
using Rei.Messages;

namespace Rei.Cnc
{
    public class Fetcher : Client
    {
        bool disposed = false;

        Dictionary<Tuple<string, string>, Request> requests = new Dictionary<Tuple<string, string>, Request>();

        public Fetcher(Server server, WebSocketClient webSocketClient, ILoggerFactory loggerFactory)
            : base(server, webSocketClient, loggerFactory)
        {
            this.Logger.LogInformation("Fetcher {0} connected", this.Id);
        }

        protected override async Task HandleMessageAsync(Message message)
        {
            switch (message)
            {
                case FetchResponse fetchResponse:
                    this.requests.Remove(new Tuple<string, string>(fetchResponse.Id, fetchResponse.Destination));

                    Client client = this.Server.LookupClient(fetchResponse.Destination);

                    if (client != null)
                    {
                        await client.SendMessageAsync(fetchResponse);
                    }

                    break;
            }
        }

        public override Task SendMessageAsync(Message message)
        {
            lock (this.requests)
            {
                if (message is FetchRequest request)
                {
                    if (this.disposed || !this.requests.TryAdd(new Tuple<string, string>(request.Id, request.Source), request))
                    {
                        return this.SendResponseAsync(new FetchResponse
                        {
                            Id = request.Id,
                            Destination = request.Source,
                            TaskId = request.TaskId,
                            StatusCode = this.disposed ? (ushort)StatusCode.FailedDependency : (ushort)StatusCode.Conflict,
                            Alias = request.Alias,
                            ContentKind = request.ContentKind,
                            Url = request.Url,
                        });
                    }
                }
            }

            return base.SendMessageAsync(message);
        }

        public override void Dispose()
        {
            if (!this.disposed)
            {
                this.Logger.LogInformation("Fetcher {0} disconnected", this.Id);

                base.Dispose();

                lock (this.requests)
                {
                    foreach (Request request in this.requests.Values)
                    {
                        if (request is FetchRequest fetchRequest)
                        {
                            _ = this.SendResponseAsync(new FetchResponse
                            {
                                Id = fetchRequest.Id,
                                Destination = fetchRequest.Source,
                                TaskId = fetchRequest.TaskId,
                                StatusCode = (ushort)StatusCode.FailedDependency,
                                Alias = fetchRequest.Alias,
                                ContentKind = fetchRequest.ContentKind,
                                Url = fetchRequest.Url,
                            });
                        }
                    }

                    this.requests.Clear();
                }
            }

            this.disposed = true;
        }

        private async Task SendResponseAsync(Response response)
        {
            lock (this.requests)
            {
                this.requests.Remove(new Tuple<string, string>(response.Id, response.Source));
            }

            Client client = this.Server.LookupClient(response.Destination);

            if (client != null)
            {
                try
                {
                    await client.SendMessageAsync(response);
                }
                catch (WebSocketException)
                {
                }
            }
        }
    }
}
