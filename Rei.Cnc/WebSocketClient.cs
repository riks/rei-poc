﻿using System;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

using Rei.Common;
using Rei.Messages;

namespace Rei.Cnc
{
    public class WebSocketClient : IDisposable
    {
        bool disposed = false;
        Server server;
        IServiceProvider serviceProvider;
        WebSocket webSocket;
        Lock webSocketLock = new Lock();

        Dictionary<string, Client> clients = new Dictionary<string, Client>();

        protected ILogger Logger
        {
            get;
            private set;
        }

        public WebSocketClient(Server server, IServiceProvider serviceProvider, ILoggerFactory loggerFactory)
        {
            this.Logger = loggerFactory.CreateLogger(this.GetType());

            this.server = server;
            this.serviceProvider = serviceProvider;
        }

        ~WebSocketClient()
        {
            this.Dispose();
        }

        public async Task Run(HttpContext context)
        {
            this.webSocket = await context.WebSockets.AcceptWebSocketAsync();
            
            while (this.webSocket.State == WebSocketState.Open)
            {
                Message message;

                try
                {
                    message = await this.webSocket.ReceiveMessageAsync();
                }
                catch (WebSocketException)
                {
                    break;
                }
                catch (Exception exception)
                {
                    this.Logger.LogError(exception, "Message handling error.");

                    continue;
                }

                if (message == null)
                {
                    continue;
                }

                switch (message)
                {
                    case HelloRequest request:
                        switch (request.ClientType)
                        {
                            case ClientType.Analyser:
                                _ = this.serviceProvider.GetService<Analyser>().ReceiveMessageAsync(request);

                                break;
                            case ClientType.Fetcher:
                                _ = this.serviceProvider.GetService<Fetcher>().ReceiveMessageAsync(request);

                                break;
                            default:
                                HelloResponse response = new HelloResponse
                                {
                                    Id = request.Id,
                                    StatusCode = (ushort)StatusCode.NotImplemented,
                                };

                                await this.SendMessageAsync(response);

                                break;
                        }

                        break;
                    default:
                        if (message.Source == null)
                        {
                            this.Logger.LogWarning("Got message without source");
                        }
                        else if (this.clients.TryGetValue(message.Source, out Client client))
                        {
                            _ = client.ReceiveMessageAsync(message);
                        }
                        else
                        {
                            this.Logger.LogWarning("Got message with unknown source");
                        }

                        break;
                }
            }
        }

        public async Task SendMessageAsync(Message message)
        {
            using (_ = await this.webSocketLock.LockAsync())
            {
                try
                {
                    await this.webSocket.SendMessageAsync(message);
                }
                catch (WebSocketException)
                {
                }
            }
        }

        public string RegisterClient(Client client)
        {
            string id = this.server.RegisterClient(client);

            lock (this.clients)
            {
                this.clients[id] = client;
            }

            return id;
        }

        public void UnregisterClient(Client client)
        {
            this.server.UnregisterClient(client);

            lock (this.clients)
            {
                this.clients.Remove(client.Id);
            }
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                this.disposed = true;

                GC.SuppressFinalize(this);

                lock (this.clients)
                {
                    Dictionary<string, Client> clients = this.clients;

                    this.clients = new Dictionary<string, Client>();

                    foreach (Client client in clients.Values)
                    {
                        client.Dispose();
                    }

                    this.clients.Clear();
                }
            }
        }
    }
}
