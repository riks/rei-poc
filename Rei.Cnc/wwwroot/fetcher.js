(function(window) {
    "use strict";

    function base64(arrayBuffer) {
        var base64 = "";
        var encodings = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

        var bytes  = new Uint8Array(arrayBuffer);
        var byteLength = bytes.byteLength;
        var byteRemainder = byteLength % 3;
        var mainLength = byteLength - byteRemainder;

        var a, b, c, d;
        var chunk;

        // Main loop deals with bytes in chunks of 3
        for (var i = 0; i < mainLength; i = i + 3) {
            // Combine the three bytes into a single integer
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18;  // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048) >> 12;    // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032) >> 6;       // 4032     = (2^6 - 1) << 6
            d = chunk & 63;                // 63       = 2^6 - 1

            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
        }

            // Deal with the remaining bytes and padding
        if (byteRemainder == 1) {
            chunk = bytes[mainLength];

            a = (chunk & 252) >> 2;  // 252 = (2^6 - 1) << 2

            // Set the 4 least significant bits to zero
            b = (chunk & 3) << 4;  // 3   = 2^2 - 1

            base64 += encodings[a] + encodings[b] + "==";
        } else if (byteRemainder == 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

            a = (chunk & 64512) >> 10;  // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008) >> 4;    // 1008  = (2^6 - 1) << 4

            // Set the 2 least significant bits to zero
            c = (chunk & 15) << 2;  // 15    = 2^4 - 1

            base64 += encodings[a] + encodings[b] + encodings[c] + "=";
        }

        return base64;
    }

    function Fetcher(url, alias) {
        this.url = url;

        this.connect();

        this.id = null;
        this.alias = alias;
    }

    Fetcher.prototype.connect = function connect() {
        if (this.websocket) {
            this.websocket.close();
        }

        this.websocket = new window.WebSocket(this.url);
        this.websocket.onopen = this.onopen.bind(this);
        this.websocket.onmessage = this.onmessage.bind(this);
        this.websocket.onclose = this.reconnect.bind(this);
    };

    Fetcher.prototype.reconnect = function reconnect() {
        window.setTimeout(this.connect.bind(this), 1000);
    };

    Fetcher.prototype.send = function send(message) {
        this.websocket.send(JSON.stringify(message));
    };

    Fetcher.prototype.onopen = function onopen(message) {
        var message = {
            "id": "hello",
            "type": "hello_request",
            "client_type": "fetcher",
            "alias": this.alias,
            "client_classes": [this.alias],
            "timestamp": Date.now() / 1000,
        }

        this.websocket.send(JSON.stringify(message));
    };

    Fetcher.prototype.onmessage = function onmessage(message) {
        var fetcher = this;

        var message = JSON.parse(message.data);

        if (message["type"] == "hello_response") {
            if (message["status_code"] == 200) {
                this.id = message["destination"];
            }
        } else if (message["type"] == "fetch_request") {
            window.console.debug("Resource", message["url"], "requested");

            var url = message["url"];

            var result = {
                "id": message["id"],
                "type": "fetch_response",
                "source": this.id,
                "task_id": message["task_id"],
                "alias": message["alias"],
                "content_kind": message["content_kind"],
                "url": url,
            };

            if (message["source"]) {
                result["destination"] = message["source"];
            }

            var statusCode = null;
            var statusText = null;
            var url = null;
            var content = null;
            var contentType = null;
            var timestamp = null;

            fetch(message["url"], {
                "cache": "no-cache",
                "credentials": "same-origin",
                "redirect": 'follow',
            }).then(function(response) {
                url = response.url;
                statusCode = response.status;
                statusText = response.statusText.trim();
                contentType = response.headers.get("Content-Type");

                if (contentType) {
                    contentType = contentType.split(";")[0];
                }

                if (!contentType) {
                    contentType = "application/octet-stream";
                }

                if (!response.ok) {
                    var error = statusCode.toString() + " " + statusText;

                    window.console.error("There has been a problem with fetch operation:", error);
                }

                return response.arrayBuffer();
            }).then(function(arrayBuffer) {
                content = base64(arrayBuffer);

                result["status_code"] = statusCode;

                if (statusText) {
                    result["status_text"] = statusText;
                }

                result["url"] = url || message["url"];
                result["content"] = content;
                result["content_type"] = contentType;
                result["timestamp"] = Date.now() / 1000;

                window.console.info("Resource", result["url"], "fetched");

                fetcher.send(result);
            }).catch(function(error) {
                if (error instanceof TypeError) {
                    if (!statusCode) {
                        statusCode = 523;
                        statusText = "Origin Is Unreachable";
                    }
                } else {
                    if (!statusCode) {
                        statusCode = 500;
                        statusText = "Internal Server Error";
                    }
                }

                statusText = statusText.trim();

                window.console.error("There has been a problem with fetch operation:\n", error);

                result["status_code"] = statusCode;

                if (statusText) {
                    result["status_text"] = statusText;
                }

                result["url"] = url || message["url"];
                result["timestamp"] = Date.now() / 1000;

                fetcher.send(result);
            });
        }
    };

    window.Fetcher = Fetcher;

})(window);