(function(window) {
    "use strict";

    window.GetFetcherCode = function GetFetcherCode() {
        var fetcherUrl = window.location.origin + "/fetcher.js";

        var apiUrl;

        if (window.location.protocol == "http:") {
            apiUrl = "ws://";
        } else if (window.location.protocol == "https:") {
            apiUrl = "wss://";
        } else {
            apiUrl = indow.location.protocol;
        }

        apiUrl += window.location.host + "/api";

        var alias = window.prompt("Enter alias") || "";

        var code = "(function(window){";
        code += "var script=window.document.createElement(\"script\");";
        code += "script.src=" + JSON.stringify(fetcherUrl) + ";";
        code += "script.onload=function(){";
        code += "var fetcher=new window.Fetcher(";
        code += JSON.stringify(apiUrl) + ",";
        code += JSON.stringify(alias) + ");";
        code += "window.console.log(fetcher);";
        code += "};";
        code += "window.document.head.appendChild(script);";
        code += "})(window);\r\n";
        
        var textArea= window.document.getElementById("fetcher-code");

        textArea.value = code;

        textArea.focus();

        textArea.setSelectionRange(0, code.length);
    };
})(window);