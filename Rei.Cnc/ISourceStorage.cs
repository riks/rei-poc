﻿using System.Threading.Tasks;

using Rei.Messages;

namespace Rei.Cnc
{
    public interface ISourceStorage
    {
        Task<bool> SourceExpiredAsync(EnsureTaskRequest request);
        Task SaveSourceAsync(FetchResponse response);
        Task<FetchResponse> LoadSourceAsync(FetchRequest request);
    }
}
