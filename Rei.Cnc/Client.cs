﻿using System;
using System.Net.WebSockets;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Rei.Common;
using Rei.Messages;

namespace Rei.Cnc
{
    public abstract class Client : IDisposable
    {
        bool disposed = false;

        public bool Connected
        {
            get => !this.disposed;
        }

        public string Id
        {
            get;
            private set;
        }

        public string Alias
        {
            get;
            private set;
        }

        public string[] ClientClasses
        {
            get;
            private set;
        }

        protected ILogger Logger
        {
            get;
            private set;
        }

        protected Server Server
        {
            get;
            private set;
        }

        protected WebSocketClient WebSocketClient
        {
            get;
            private set;
        }

        public Client(Server server, WebSocketClient webSocketClient, ILoggerFactory loggerFactory)
        {
            this.Logger = loggerFactory.CreateLogger(this.GetType());
            this.Server = server;
            this.WebSocketClient = webSocketClient;

            this.Id = this.WebSocketClient.RegisterClient(this);
        }

        public async Task ReceiveMessageAsync(Message message)
        {
            try
            {
                if (message is HelloRequest request)
                {
                    this.Alias = request.Alias;
                    this.ClientClasses = new string[request.ClientClasses?.Count ?? 0];

                    if (this.ClientClasses.Length > 0)
                    {
                        request.ClientClasses.CopyTo(this.ClientClasses, 0);
                    }

                    HelloResponse response = new HelloResponse
                    {
                        Id = request.Id,
                        Destination = this.Id,
                        StatusCode = (ushort)StatusCode.OK,
                    };

                    await this.SendMessageAsync(response);
                }

                await this.HandleMessageAsync(message);

                if (message is ByeMessage)
                {
                    this.Dispose();
                }
            }
            catch (WebSocketException)
            {
            }
            catch (Exception exception)
            {
                this.Logger.LogError(exception, "Unhandled exception");
            }
        }

        protected abstract Task HandleMessageAsync(Message message);

        public virtual async Task SendMessageAsync(Message message)
        {
            try
            {
                await this.WebSocketClient.SendMessageAsync(message);
            }
            catch (WebSocketException)
            {
                this.Dispose();

                throw;
            }
        }

        ~Client()
        {
            this.Dispose();
        }

        public virtual void Dispose()
        {
            if (!this.disposed)
            {
                this.disposed = true;

                GC.SuppressFinalize(this);

                this.WebSocketClient.UnregisterClient(this);                
            }            
        }
    }
}
