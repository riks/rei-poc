﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

using Npgsql;

using Rei.Common;
using Rei.Messages;

namespace Rei.Cnc
{
    public class PgSqlSourceStorage : ISourceStorage, IDisposable
    {
        Lock connectionLock = new Lock();
        NpgsqlConnection connection;

        protected ILogger Logger
        {
            get;
            private set;
        }

        public PgSqlSourceStorage(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            this.Logger = loggerFactory.CreateLogger(this.GetType());

            NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder(configuration["PostgreSQL"])
            {
                Pooling = false,
                ApplicationName = "REI C'n'C Source Storage",
            };

            this.connection = new NpgsqlConnection(builder.ToString());
        }

        public async Task<bool> SourceExpiredAsync(EnsureTaskRequest request)
        {
            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"SELECT
    ""update""
FROM ""rdw"".""source""
WHERE ""alias"" = @alias AND ""content_kind"" = @content_kind AND ""url"" = @url;";

                    command.Parameters.AddWithValue("alias", request.Alias);
                    command.Parameters.AddWithValue("content_kind", request.ContentKind);
                    command.Parameters.AddWithValue("url", request.Url);

                    return (bool?)await command.ExecuteScalarAsync() ?? true;
                }
            }
        }

        public async Task SaveSourceAsync(FetchResponse response)
        {   if (response.StatusCode < 200 || response.StatusCode >= 300  || response.Content == null && response.ContentKind == null)
            {
                return;
            }

            string hash = Sha3.Sha3_256.CalculateHex(response.Content);

            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"INSERT INTO ""rdw"".""raw""(
    ""hash"",
    ""content_type"",
    ""content""
) VALUES(
    @hash,
    @content_type,
    @content
) ON CONFLICT DO NOTHING;";

                    command.Parameters.AddWithValue("hash", hash);
                    command.Parameters.AddWithValue("content_type", response.ContentType);
                    command.Parameters.AddWithValue("content", response.Content);

                    await command.ExecuteNonQueryAsync();
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"INSERT INTO ""rdw"".""source"" (
    ""alias"",
    ""content_kind"",
    ""url"",
    ""content_type"",
    ""hash"",
    ""update""
) VALUES(
    @alias,
    @content_kind,
    @url,
    @content_type,
    @hash,
    false
) ON CONFLICT(""alias"", ""url"") DO UPDATE SET
    ""content_kind"" = ""excluded"".""content_kind"",
    ""content_type"" = ""excluded"".""content_type"",
    ""hash"" = ""excluded"".""hash"",
    ""updated_at"" = ""timezone""('UTC', ""clock_timestamp""()),
    ""update"" = false;";

                    command.Parameters.AddWithValue("alias", response.Alias);
                    command.Parameters.AddWithValue("content_kind", response.ContentKind);
                    command.Parameters.AddWithValue("url", response.Url);
                    command.Parameters.AddWithValue("content_type", response.ContentType);
                    command.Parameters.AddWithValue("hash", hash);

                    await command.ExecuteNonQueryAsync();

                    this.Logger.LogDebug("Source {0} saved", response.Url);
                }
            }
        }

        public async Task<FetchResponse> LoadSourceAsync(FetchRequest request)
        {
            using (_ = await this.connectionLock.LockAsync())
            {
                if (this.connection.State == ConnectionState.Closed)
                {
                    await this.connection.OpenAsync();
                }

                using (NpgsqlCommand command = this.connection.CreateCommand())
                {
                    command.CommandText = @"SELECT
    ""s"".""alias"",
    ""s"".""content_kind"",
    ""s"".""url"",
    ""s"".""content_type"",
    ""r"".""content""
FROM ""rdw"".""source"" ""s""
INNER JOIN ""rdw"".""raw"" ""r"" ON ""r"".""hash"" = ""s"".""hash"" AND ""r"".""content_type"" = ""s"".""content_type""
WHERE ""s"".""alias"" = @alias AND ""s"".""content_kind"" = @content_kind AND ""s"".""url"" = @url AND NOT ""s"".""update"";";

                    command.Parameters.AddWithValue("alias", request.Alias);
                    command.Parameters.AddWithValue("content_kind", request.ContentKind);
                    command.Parameters.AddWithValue("url", request.Url);

                    using (DbDataReader reader = await command.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            string alias = (string)reader["alias"];
                            string contentKind = (string)reader["content_kind"];
                            string url = (string)reader["url"];
                            string contentType = (string)reader["content_type"];
                            byte[] content = (byte[])reader["content"];

                            this.Logger.LogDebug("Source {0} loaded", request.Url);

                            return new FetchResponse
                            {
                                Id = request.Id,
                                Destination = request.Source,
                                TaskId = request.TaskId,
                                StatusCode = (ushort)StatusCode.NotModified,
                                Alias = alias,
                                ContentKind = contentKind,
                                Url = url,
                                ContentType = contentType,
                                Content = content,
                            };
                        }

                        return null;
                    }
                }
            }
        }

        ~PgSqlSourceStorage()
        {
            this.Dispose();
        }

        public void Dispose()
        {
            if (this.connection.State != ConnectionState.Closed)
            {
                this.connection.Close();
            }

            GC.SuppressFinalize(this);
        }
    }
}
