﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;

using Microsoft.AspNetCore.Http;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Rei.Common;

namespace Rei.Cnc
{
    public class Server
    {
        IServiceProvider serviceProvider;

        ConcurrentDictionary<string, Client> clients = new ConcurrentDictionary<string, Client>();
        HashSet<Fetcher> fetchers = new HashSet<Fetcher>();

        protected ILogger Logger
        {
            get;
            private set;
        }

        public Server(IServiceProvider serviceProvider, IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            this.Logger = loggerFactory.CreateLogger(this.GetType());

            this.serviceProvider = serviceProvider;
        }

        public async Task WebSocketApi(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await this.ErrorHandler(context, 426);

                return;
            }

            using (IServiceScope scope = this.serviceProvider.CreateScope())
            {
                using (WebSocketClient client = scope.ServiceProvider.GetService<WebSocketClient>())
                {
                    await client.Run(context);
                }
            }
        }

        public string RegisterClient(Client client)
        {
            string id;

            do
            {
                Random random = new Random();

                StringBuilder builder = new StringBuilder(16);

                while (builder.Length < 16)
                {
                    builder.Append("abcdefghijklmnopqrstuvwxyz"[random.Next(26)]);
                }

                id = builder.ToString();
            }
            while (!this.clients.TryAdd(id, client));

            if (client is Fetcher fetcher)
            {
                lock (this.fetchers)
                {
                    this.fetchers.Add(fetcher);
                }
            }

            return id;
        }

        public void UnregisterClient(Client client)
        {
            this.clients.TryRemove(client.Id, out _);

            if (client is Fetcher fetcher)
            {
                lock (this.fetchers)
                {
                    this.fetchers.Remove(fetcher);
                }
            }
        }

        public Fetcher AcquireFetcher(Analyser analyser, string alias, IList<string> clientClasses)
        {
            lock (this.fetchers)
            {
                Fetcher fetcher = null;

                foreach (Fetcher candidate in this.fetchers)
                {
                    if (candidate.Connected)
                    {
                        fetcher = candidate;
                    }

                    break;
                }

                if (fetcher != null)
                {
                    this.fetchers.Remove(fetcher);
                }

                return fetcher;
            }
        }

        public void ReleaseFetcher(Fetcher fetcher)
        {
            if (fetcher != null && fetcher.Connected)
            {
                lock (this.fetchers)
                {
                    this.fetchers.Add(fetcher);
                }
            }
        }

        public Client LookupClient(string id)
        {
            if (this.clients.TryGetValue(id, out Client client))
            {
                return client;
            }

            return null;
        }

        public Task ErrorHandler(HttpContext context) => this.ErrorHandler(context, context.Response.StatusCode);

        public Task ErrorHandler(HttpContext context, int statusCode)
        {
            return ErrorHandler(context, statusCode, ((StatusCode)statusCode).StatusText(false));
        }

        public Task ErrorHandler(HttpContext context, int statusCode, string statusText)
        {
            return context.Response.WriteAsync(string.Format("{0} {1}", statusCode, statusText));
        }
    }
}
