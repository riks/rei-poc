﻿using System.Threading.Tasks;

using Rei.Messages;

namespace Rei.Cnc
{
    public interface ITaskStorage
    {
        Task<EnsureTaskResponse> EnqueueTask(EnsureTaskRequest request);
        Task<AnalyseRequest> DequeuTaskAsync(string alias);
        Task CompleteTaskAsync(AnalyseResponse response);
        Task<LockResponse> LockTaskAsync(LockRequest request);
        Task UnlockTaskAsync(string alias, long taskId);
    }
}
