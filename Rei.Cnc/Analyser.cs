﻿using System;
using System.Threading.Tasks;
using System.Net.WebSockets;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;

using Rei.Common;
using Rei.Messages;

namespace Rei.Cnc
{
    public class Analyser : Client
    {
        bool disposed = false;
        private WeakReference<Fetcher> fetcher;
        HashSet<Tuple<string, long>> locks = new HashSet<Tuple<string, long>>();

        ITaskStorage taskStorage;
        ISourceStorage sourceStorage;

        public Fetcher Fetcher
        {
            get
            {
                if (this.fetcher != null && this.fetcher.TryGetTarget(out Fetcher fetcher))
                {
                    if (fetcher.Connected)
                    {
                        return fetcher;
                    }

                    this.Server.ReleaseFetcher(fetcher);

                    this.fetcher = null;
                }

                return null;
            }
            set
            {
                if (this.fetcher != null && this.fetcher.TryGetTarget(out Fetcher fetcher))
                {
                    this.Server.ReleaseFetcher(fetcher);
                }

                this.fetcher = new WeakReference<Fetcher>(value);
            }
        }

        public Analyser(Server server, WebSocketClient webSocketClient, ILoggerFactory loggerFactory, ITaskStorage taskStorage, ISourceStorage sourceStorage)
            : base(server, webSocketClient, loggerFactory)
        {
            this.taskStorage = taskStorage;
            this.sourceStorage = sourceStorage;

            this.Logger.LogInformation("Analyser {0} connected", this.Id);
        }

        protected override async Task HandleMessageAsync(Message message)
        {

            switch (message)
            {
                case HelloRequest _:
                    _ = this.SendAnalyseRequestAsync();

                    break;
                case LockRequest lockRequest:
                    LockResponse lockResponse = await this.taskStorage.LockTaskAsync(lockRequest);

                    if (lockResponse.Locked)
                    {
                        lock (this.locks)
                        {
                            this.locks.Add(new Tuple<string, long>(lockRequest.Alias, lockRequest.TaskId));
                        }
                    }

                    await this.SendMessageAsync(lockResponse);

                    break;
                case UnlockMessage unlockMessage:
                    await this.taskStorage.UnlockTaskAsync(unlockMessage.Alias, unlockMessage.TaskId);

                    lock (this.locks)
                    {
                        this.locks.Remove(new Tuple<string, long>(unlockMessage.Alias, unlockMessage.TaskId));
                    }

                    break;
                case FetchRequest fetchRequest:
                    FetchResponse fetchResponse = await this.sourceStorage.LoadSourceAsync(fetchRequest);

                    if (fetchResponse != null)
                    {
                        await this.SendMessageAsync(fetchResponse);

                        break;
                    }

                    this.AcquireFetcher(fetchRequest);

                    Fetcher fetcher = this.Fetcher;

                    if (fetcher == null)
                    {
                        await this.SendMessageAsync(new FetchResponse
                        {
                            Id = fetchRequest.Id,
                            Destination = fetchRequest.Source,
                            TaskId = fetchRequest.TaskId,
                            StatusCode = (ushort)StatusCode.FailedDependency,
                            Alias = fetchRequest.Alias,
                            ContentKind = fetchRequest.ContentKind,
                            Url = fetchRequest.Url,
                        });

                        break;
                    }

                    fetchRequest.Destination = this.Fetcher.Id;

                    try
                    {
                        await fetcher.SendMessageAsync(fetchRequest);
                    }
                    catch (WebSocketException)
                    {
                        this.ReleaseFetcher();

                        await this.SendMessageAsync(new FetchResponse
                        {
                            Id = fetchRequest.Id,
                            Destination = fetchRequest.Source,
                            TaskId = fetchRequest.TaskId,
                            StatusCode = (ushort)StatusCode.FailedDependency,
                            Alias = fetchRequest.Alias,
                            ContentKind = fetchRequest.ContentKind,
                            Url = fetchRequest.Url,
                        });
                    }

                    break;
                case EnsureTaskRequest ensureTaskRequest:
                    await this.SendMessageAsync(await this.taskStorage.EnqueueTask(ensureTaskRequest));

                    break;
                case AnalyseResponse analyseResponse:
                    if (analyseResponse.StatusCode == 100 || analyseResponse.StatusCode == 200)
                    {
                        await this.taskStorage.CompleteTaskAsync(analyseResponse);
                    }

                    if (analyseResponse.StatusCode >= 200)
                    {
                        this.Logger.LogDebug("Analyser {0} finished", this.Id);

                        _ = this.SendAnalyseRequestAsync();
                    }

                    break;
                case BadClientMessage badClientMessage:
                    Fetcher badFetcher = this.Fetcher;

                    if (badFetcher != null)
                    {
                        badClientMessage.Destination = badFetcher.Id;

                        try
                        {
                            await badFetcher.SendMessageAsync(badClientMessage);
                        }
                        catch (WebSocketException)
                        {
                        }
                    }

                    this.ReleaseFetcher();

                    this.Logger.LogDebug("Analyser {0} complained about fetcher", this.Id);

                    break;

                case ByeMessage buyMessage:
                    this.Dispose();

                    break;
            }
        }

        public override async Task SendMessageAsync(Message message)
        {
            if (message is FetchResponse response)
            {
                await this.sourceStorage.SaveSourceAsync(response);
            }

            await base.SendMessageAsync(message);
        }

        protected void AcquireFetcher(FetchRequest request)
        {
            Fetcher fetcher;

            if (this.Fetcher == null)
            {
                fetcher = this.Server.AcquireFetcher(this, request.Alias, request.ClientClasses);

                if (fetcher != null)
                {
                    this.Logger.LogDebug("Analyser {0} acquired fetcher {1}", this.Id, fetcher.Id);
                }

                this.Fetcher = fetcher;
            }
        }

        protected void ReleaseFetcher()
        {
            Fetcher fetcher = this.Fetcher;

            this.Fetcher = null;

            if (fetcher != null)
            {
                this.Server.ReleaseFetcher(fetcher);

                this.Logger.LogDebug("Analyser {0} released fetcher {1}", this.Id, fetcher.Id);
            }
        }

        public override void Dispose()
        {
            if (!this.disposed)
            {
                this.Logger.LogInformation("Analyser {0} disconnected", this.Id);

                base.Dispose();

                _ = this.UnlockAllAsync();

                this.ReleaseFetcher();
            }

            this.disposed = true;
        }

        private async Task SendAnalyseRequestAsync()
        {
            if (this.disposed)
            {
                return;
            }

            AnalyseRequest analyseRequest = await this.taskStorage.DequeuTaskAsync(this.Alias);

            if (analyseRequest == null)
            {
                await Task.Delay(TimeSpan.FromSeconds(5));

                if (!this.disposed)
                {
                    _ = this.SendAnalyseRequestAsync();
                }

                return;
            }

            analyseRequest.Destination = this.Id;

            await this.SendMessageAsync(analyseRequest);

            this.Logger.LogDebug("Sent analyse request to analyser {0}", this.Id);
        }

        private async Task UnlockAllAsync()
        {
            HashSet<Tuple<string, long>> locks;

            lock (this.locks)
            {
                locks = this.locks;

                this.locks = new HashSet<Tuple<string, long>>();
            }

            foreach (Tuple<string, long> item in locks)
            {
                await this.taskStorage.UnlockTaskAsync(item.Item1, item.Item2);
            }

            locks.Clear();
        }
    }
}
