﻿using System;
using System.Text;

using Org.BouncyCastle.Crypto.Digests;

namespace Rei.Common
{
    public struct Sha3
    {
        static string hexDigits = "0123456789abcdef";

        public static Sha3 Sha3_224
        {
            get => new Sha3(224);
        }

        public static Sha3 Sha3_256
        {
            get => new Sha3(256);
        }

        public static Sha3 Sha3_384
        {
            get => new Sha3(384);
        }

        public static Sha3 Sha3_512
        {
            get => new Sha3(512);
        }

        private int bitLength;

        public Sha3(int bitLength)
        {
            this.bitLength = bitLength;
        }

        public byte[] Calculate(byte[] data, int offset, int length)
        {
            Sha3Digest hasher = new Sha3Digest(this.bitLength == 0 ? 256 : this.bitLength);

            byte[] output = new byte[hasher.GetDigestSize()];

            hasher.BlockUpdate(data, offset, length);
            hasher.DoFinal(output, 0);

            return output;
        }

        public byte[] Calculate(ArraySegment<byte> data) => this.Calculate(data.Array, data.Offset, data.Count);
        public byte[] Calculate(byte[] data) => this.Calculate(data, 0, data.Length);
        public byte[] Calculate(string data) => this.Calculate(Encoding.UTF8.GetBytes(data));

        public string CalculateHex(byte[] data, int offset, int length)
        {
            byte[] digest = this.Calculate(data, offset, length);

            StringBuilder builder = new StringBuilder(digest.Length * 2);

            for (int index = 0; index < digest.Length; index++)
            {
                builder.Append(hexDigits[digest[index] >> 4]);
                builder.Append(hexDigits[digest[index] & 0xf]);
            }

            return builder.ToString();
        }

        public string CalculateHex(ArraySegment<byte> data) => this.CalculateHex(data.Array, data.Offset, data.Count);
        public string CalculateHex(byte[] data) => this.CalculateHex(data, 0, data.Length);
        public string CalculateHex(string data) => this.CalculateHex(Encoding.UTF8.GetBytes(data));
    }
}
