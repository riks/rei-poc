﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Rei.Common
{
    public static class StatusCodeExtensions
    {
        static string[] emptyStatusTexts = new string[0];
        static Dictionary<StatusCode, string[]> statusTextDictionary;

        static StatusCodeExtensions()
        {
            statusTextDictionary = new Dictionary<StatusCode, string[]>();

            Type statusCodeType = typeof(StatusCode);
            StatusCode[] values = (StatusCode[])Enum.GetValues(statusCodeType);

            foreach (StatusCode value in values)
            {
                MemberInfo memberInfo = statusCodeType.GetMember(Enum.GetName(statusCodeType, value))[0];
                EnumMemberAttribute[] attributes = (EnumMemberAttribute[])memberInfo.GetCustomAttributes(typeof(EnumMemberAttribute), false);

                string[] statusTexts = new string[attributes.Length];

                int count = 0;

                foreach (EnumMemberAttribute attribute in attributes)
                {
                    if (!string.IsNullOrWhiteSpace(attribute.Value))
                    {
                        statusTexts[count] = attribute.Value;

                        count += 1;
                    }
                }

                Array.Resize(ref statusTexts, count);

                statusTextDictionary[value] = statusTexts;
            }

        }

        public static string[] StatusTexts(this StatusCode code)
        {
            if (statusTextDictionary.TryGetValue(code, out string[] statusTexts))
            {
                return statusTexts;
            }

            return emptyStatusTexts;
        }

        public static string StatusText(this StatusCode code, bool strict=true)
        {
            string[] statusTexts = code.StatusTexts();

            if (statusTexts.Length > 0)
            {
                return statusTexts[0];
            }

            if (strict)
            {
                return null;
            }

            if (Enum.IsDefined(typeof(StatusCode), code))
            {
                return Enum.GetName(typeof(StatusCode), code);
            }

            return string.Empty;
        }
    }
}
