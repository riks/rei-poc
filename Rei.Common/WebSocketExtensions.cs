﻿using System;
using System.IO;
using System.Threading;
using System.Net.WebSockets;
using System.Threading.Tasks;

using Rei.Messages;

namespace Rei.Common
{
    public static class WebSocketExtensions
    {
        public static async Task<Message> ReceiveMessageAsync(this WebSocket webSocket)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                byte[] buffer = new byte[4096];

                WebSocketReceiveResult receiveResult;

                do
                {
                    buffer.Initialize();

                    receiveResult = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer, 0, buffer.Length), CancellationToken.None);

                    switch (receiveResult.MessageType)
                    {
                        case WebSocketMessageType.Close:
                            await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);

                            continue;
                        case WebSocketMessageType.Binary:
                            await webSocket.CloseAsync(WebSocketCloseStatus.InvalidMessageType, "Cannot accept binary frame", CancellationToken.None);

                            continue;
                    }

                    stream.Write(buffer, 0, receiveResult.Count);

                    if (receiveResult.EndOfMessage)
                    {
                        stream.Position = 0;

                        return Message.ReadFrom(stream);
                    }
                }
                while (!receiveResult.EndOfMessage);
            }

            return null;
        }

        public static async Task SendMessageAsync(this WebSocket webSocket, Message message)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                message.WriteTo(stream);

                stream.Position = 0;

                byte[] buffer = stream.ToArray();

                await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, buffer.Length), WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }
    }
}
