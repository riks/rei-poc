﻿using System.Runtime.Serialization;

namespace Rei.Common
{
    public enum StatusCode : ushort
    {
        [EnumMember(Value = "Continue")]
        Continue = 100,

        [EnumMember(Value = "Switching Protocols")]
        SwitchingProtocols = 101,

        [EnumMember(Value = "Processing")]
        Processing = 102,

        [EnumMember(Value = "Early Hints")]
        EarlyHints = 103,

        [EnumMember(Value = "OK")]
        OK = 200,

        [EnumMember(Value = "Created")]
        Created = 201,

        [EnumMember(Value = "Accepted")]
        Accepted = 202,

        [EnumMember(Value = "Non-Authoritative Information")]
        NonAuthoritativeInformation = 203,

        [EnumMember(Value = "No Content")]
        NoContent = 204,

        [EnumMember(Value = "Reset Content")]
        ResetContent = 205,

        [EnumMember(Value = "Partial Content")]
        PartialContent = 206,

        [EnumMember(Value = "Multi-Status")]
        MultiStatus = 207,

        [EnumMember(Value = "Already Reported")]
        AlreadyReported = 208,

        [EnumMember(Value = "IM Used")]
        IMUsed = 226,

        [EnumMember(Value = "Multiple Choices")]
        MultipleChoices = 300,

        [EnumMember(Value = "Moved Permanently")]
        MovedPermanently = 301,

        [EnumMember(Value = "Moved Temporarily")]
        MovedTemporarily = 302,

        [EnumMember(Value = "See Other")]
        SeeOther = 303,

        [EnumMember(Value = "Not Modified")]
        NotModified = 304,

        [EnumMember(Value = "Use Proxy")]
        UseProxy = 305,

        [EnumMember(Value = "Switch Proxy")]
        SwitchProxy = 306,

        [EnumMember(Value = "Temporary Redirect")]
        TemporaryRedirect = 307,

        [EnumMember(Value = "Permanent Redirect")]
        PermanentRedirect = 308,

        [EnumMember(Value = "Bad Request")]
        BadRequest = 400,

        [EnumMember(Value = "Unauthorized")]
        Unauthorized = 401,

        [EnumMember(Value = "Payment Required")]
        PaymentRequired = 402,

        [EnumMember(Value = "Forbidden")]
        Forbidden = 403,

        [EnumMember(Value = "Not Found")]
        NotFound = 404,

        [EnumMember(Value = "Method Not Allowed")]
        MethodNotAllowed = 405,

        [EnumMember(Value = "Not Acceptable")]
        NotAcceptable = 406,

        [EnumMember(Value = "Proxy Authentication Required")]
        ProxyAuthenticationRequired = 407,

        [EnumMember(Value = "Request Timeout")]
        RequestTimeout = 408,

        [EnumMember(Value = "Conflict")]
        Conflict = 409,

        [EnumMember(Value = "Gone")]
        Gone = 410,

        [EnumMember(Value = "Length Required")]
        LengthRequired = 411,

        [EnumMember(Value = "Precondition Failed")]
        PreconditionFailed = 412,

        [EnumMember(Value = "Payload Too Large")]
        PayloadTooLarge = 413,

        [EnumMember(Value = "URI Too Long")]
        URITooLong = 414,

        [EnumMember(Value = "Unsupported Media Type")]
        UnsupportedMediaType = 415,

        [EnumMember(Value = "Range Not Satisfiable")]
        RangeNotSatisfiable = 416,

        [EnumMember(Value = "Expectation Failed")]
        ExpectationFailed = 417,

        [EnumMember(Value = "I'm a teapot")]
        ImAteapot = 418,

        [EnumMember(Value = "Misdirected Request")]
        MisdirectedRequest = 421,

        [EnumMember(Value = "Unprocessable Entity")]
        UnprocessableEntity = 422,

        [EnumMember(Value = "Locked")]
        Locked = 423,

        [EnumMember(Value = "Failed Dependency")]
        FailedDependency = 424,

        [EnumMember(Value = "Upgrade Required")]
        UpgradeRequired = 426,

        [EnumMember(Value = "Precondition Required")]
        PreconditionRequired = 428,

        [EnumMember(Value = "Too Many Requests")]
        TooManyRequests = 429,

        [EnumMember(Value = "Request Header Fields Too Large")]
        RequestHeaderFieldsTooLarge = 431,

        [EnumMember(Value = "Retry With")]
        RetryWith = 449,

        [EnumMember(Value = "Unavailable For Legal Reasons")]
        UnavailableForLegalReasons = 451,

        [EnumMember(Value = "Internal Server Error")]
        InternalServerError = 500,

        [EnumMember(Value = "Not Implemented")]
        NotImplemented = 501,

        [EnumMember(Value = "Bad Gateway")]
        BadGateway = 502,

        [EnumMember(Value = "Service Unavailable")]
        ServiceUnavailable = 503,

        [EnumMember(Value = "Gateway Timeout")]
        GatewayTimeout = 504,

        [EnumMember(Value = "HTTP Version Not Supported")]
        HTTPVersionNotSupported = 505,

        [EnumMember(Value = "Variant Also Negotiates")]
        VariantAlsoNegotiates = 506,

        [EnumMember(Value = "Insufficient Storage")]
        InsufficientStorage = 507,

        [EnumMember(Value = "Loop Detected")]
        LoopDetected = 508,

        [EnumMember(Value = "Bandwidth Limit Exceeded")]
        BandwidthLimitExceeded = 509,

        [EnumMember(Value = "Not Extended")]
        NotExtended = 510,

        [EnumMember(Value = "Network Authentication Required")]
        NetworkAuthenticationRequired = 511,

        [EnumMember(Value = "Unknown Error")]
        UnknownError = 520,

        [EnumMember(Value = "Web Server Is Down")]
        WebServerIsDown = 521,

        [EnumMember(Value = "Connection Timed Out")]
        ConnectionTimedOut = 522,

        [EnumMember(Value = "Origin Is Unreachable")]
        OriginIsUnreachable = 523,

        [EnumMember(Value = "A Timeout Occurred")]
        TimeoutOccurred = 524,

        [EnumMember(Value = "SSL Handshake Failed")]
        SSLHandshakeFailed = 525,

        [EnumMember(Value = "Invalid SSL Certificate")]
        InvalidSSLCertificate = 526,

        [EnumMember(Value = "Network Read Timeout")]
        NetworkReadTimeout = 598,
    }
}
