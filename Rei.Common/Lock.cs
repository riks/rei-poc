﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Rei.Common
{
    public class Lock
    {
        Guard guard = new Guard();

        class Guard : IDisposable
        {
            volatile TaskCompletionSource<IDisposable> source;

            public bool Locked
            {
                get => !this.source.Task.IsCompleted;
            }

            public Guard()
            {
                this.source = new TaskCompletionSource<IDisposable>();

                this.source.SetResult(this);
            }

            public async Task<IDisposable> LockAsync()
            {
                TaskCompletionSource<IDisposable> oldSource = this.source;
                TaskCompletionSource<IDisposable> newSource = new TaskCompletionSource<IDisposable>();

                while (true)
                {
                    IDisposable result = await oldSource.Task;

                    TaskCompletionSource<IDisposable> source = Interlocked.CompareExchange(ref this.source, newSource, oldSource);

                    if (Object.ReferenceEquals(source, oldSource))
                    {
                        return result;
                    }
                    else
                    {
                        oldSource = source;
                    }
                }
            }

            public void Dispose() => this.source.SetResult(this);
        }

        public Task<IDisposable> LockAsync() => this.guard.LockAsync();
    }
}
