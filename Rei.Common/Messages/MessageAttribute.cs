﻿using System;

namespace Rei.Messages
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class MessageAttribute : Attribute
    {
        public string MessageType
        {
            get;
            protected set;
        }

        public MessageAttribute(string messageType)
        {
            if (string.IsNullOrWhiteSpace(messageType ?? throw new ArgumentNullException(nameof(messageType))))
            {
                throw new ArgumentException("Invalid message type.", nameof(messageType));
            }

            this.MessageType = messageType.Trim();
        }
    }
}
