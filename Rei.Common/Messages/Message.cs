﻿using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Rei.Messages
{
    public class Message
    {
        [ThreadStatic]
        static Dictionary<string, HashSet<Type>> messageTypes;

        static JsonSerializer serializer;
        static JsonSerializerSettings serializerSettings;

        protected static JsonSerializer Serializer
        {
            get
            {
                if (serializer == null)
                {
                    serializer = JsonSerializer.Create(SerializerSettings);
                }

                return serializer;
            }
        }

        protected static JsonSerializerSettings SerializerSettings
        {
            get
            {
                if (serializerSettings == null)
                {
                    serializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver = new DefaultContractResolver
                        {
                            NamingStrategy = new SnakeCaseNamingStrategy(),
                        },
                        Converters = new JsonConverter[]
                        {
                            new BinaryConverter(),
                            new DateTimeConverter(),
                            new StringEnumConverter(false)
                            {
                                AllowIntegerValues = false,
                            },
                        },
                    };
                }

                return serializerSettings;
            }
        }

        string id;
        string type;
        DateTimeOffset? timestamp;

        public static Message ReadFrom(Stream stream)
        {
            if (messageTypes == null)
            {
                messageTypes = new Dictionary<string, HashSet<Type>>();
            }

            JObject jObject;

            using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true, 256, true))
            {
                using (JsonTextReader jsonReader = new JsonTextReader(streamReader))
                {
                    jObject = (JObject)JToken.ReadFrom(jsonReader);
                }
            }

            string messageType = ((string)jObject["type"])?.Trim();

            if (string.IsNullOrWhiteSpace(messageType))
            {
                throw new InvalidDataException("Message type is not defined.");
            }

            if (messageTypes.TryGetValue(messageType, out HashSet<Type> set))
            {
                foreach (Type type in set)
                {
                    try
                    {
                        return (Message)jObject.ToObject(type, Serializer);
                    }
                    catch
                    {
                    }
                }
            }

            Assembly entryAssembly = Assembly.GetEntryAssembly();
            HashSet<Assembly> assemblies = new HashSet<Assembly> { entryAssembly };

            foreach (AssemblyName name in entryAssembly.GetReferencedAssemblies())
            {
                assemblies.Add(Assembly.Load(name));
            }

            foreach (Assembly assembly in assemblies)
            {
                foreach (TypeInfo typeInfo in assembly.DefinedTypes)
                {
                    if (!typeof(Message).IsAssignableFrom(typeInfo))
                    {
                        continue;
                    }

                    string type = typeInfo.GetCustomAttribute<MessageAttribute>(false)?.MessageType;

                    if (type != messageType)
                    {
                        continue;
                    }

                    if (set == null)
                    {
                        set = new HashSet<Type>();
                    }

                    set.Add(typeInfo);
                }
            }

            if (set != null)
            {
                messageTypes[messageType] = set;

                foreach (Type type in set)
                {
                    try
                    {
                        return (Message)jObject.ToObject(type, Serializer);
                    }
                    catch
                    {
                    }
                }
            }

            Message result = jObject.ToObject<Message>(Serializer);

            result.type = messageType;

            return result;
        }

        public static T ReadFrom<T>(Stream stream) where T : Message
        {
            if (messageTypes == null)
            {
                messageTypes = new Dictionary<string, HashSet<Type>>();
            }

            JObject jObject;

            using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8, true, 256, true))
            {
                using (JsonTextReader jsonReader = new JsonTextReader(streamReader))
                {
                    jObject = (JObject)JToken.ReadFrom(jsonReader);
                }
            }

            string messageType = ((string)jObject["type"])?.Trim();

            if (string.IsNullOrWhiteSpace(messageType))
            {
                throw new InvalidDataException("Message type is not defined.");
            }

            string type = typeof(T).GetCustomAttribute<MessageAttribute>(false)?.MessageType;

            if (type != null && messageType != type)
            {
                throw new InvalidCastException("Invalid message type.");
            }

            T result = jObject.ToObject<T>(Serializer);

            if (messageType == result.Type)
            {
                if (!messageTypes.TryGetValue(messageType, out HashSet<Type>  set))
                {
                    set = new HashSet<Type>();
                }

                set.Add(typeof(T));

                messageTypes[messageType] = set;

                return result;
            }

            throw new InvalidCastException("Invalid message type.");
        }

        public static bool TryReadFrom(Stream stream, out Message result)
        {
            try
            {
                result = ReadFrom(stream);

                return true;
            }
            catch
            {
                result = null;
            }

            return false;
        }

        public static bool TryReadFrom<T>(Stream stream, out T result) where T : Message
        {
            try
            {
                result = ReadFrom<T>(stream);

                return true;
            }
            catch
            {
                result = null;
            }

            return false;
        }

        [JsonProperty(Required = Required.Always)]
        public string Id
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.id))
                {
                    Random random = new Random();

                    StringBuilder builder = new StringBuilder(16);

                    builder.Append('$');
                    
                    while (builder.Length < 16)
                    {
                        builder.Append("abcdefghijklmnopqrstuvwxyz"[random.Next(26)]);
                    }

                    this.id = builder.ToString();
                }

                return this.id;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Invalid message Id.", nameof(value));
                }

                this.id = value;
            }
        }

        [JsonProperty(Required = Required.Always)]
        public virtual string Type
        {
            get
            {
                if (this.type == null)
                {
                    this.type = this.GetMessageType();
                }

                return this.type;
            }
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public string Source
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public string Destination
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public DateTimeOffset Timestamp
        {
            get
            {
                if (!this.timestamp.HasValue)
                {
                    this.timestamp = DateTimeOffset.Now;
                }

                return this.timestamp.Value;
            }
            set
            {
                this.timestamp = value;
            }
        }

        public void WriteTo(Stream stream)
        {
            JObject jObject = JObject.FromObject(this, Serializer);

            using (StreamWriter streamWriter = new StreamWriter(stream, new UTF8Encoding(false), 4096, true))
            {
                using (JsonTextWriter jsonWriter = new JsonTextWriter(streamWriter))
                {
                    jObject.WriteTo(jsonWriter);

                    jsonWriter.Flush();
                }
            }
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, SerializerSettings);
        }

        private string GetMessageType()
        {
            MessageAttribute attribute = this.GetType().GetCustomAttribute<MessageAttribute>(false);

            return attribute?.MessageType ?? throw new NullReferenceException("Message type is undefined.");
        }
    }
}
