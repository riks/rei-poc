﻿using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("lock_response")]
    public class LockResponse : Response
    {
        [JsonProperty(Required = Required.Always)]
        public long TaskId
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Alias
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public bool Locked
        {
            get;
            set;
        }
    }
}
