﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("hello_request")]
    public class HelloRequest : Message
    { 
        [JsonProperty(Required = Required.Always)]
        public ClientType ClientType
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public string Alias
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public IList<string> ClientClasses
        {
            get;
            set;
        }
    }
}
