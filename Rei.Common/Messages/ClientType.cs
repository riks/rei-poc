﻿using System.Runtime.Serialization;

namespace Rei.Messages
{
    public enum ClientType
    {
        [EnumMember(Value = "analyser")]
        Analyser,

        [EnumMember(Value = "fetcher")]
        Fetcher,
    }
}
