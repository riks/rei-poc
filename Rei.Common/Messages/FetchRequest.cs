﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("fetch_request")]
    public class FetchRequest : Request
    {
        [JsonProperty(Required = Required.Always)]
        public long TaskId
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Alias
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public IList<string> ClientClasses
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string ContentKind
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Url
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public long? ParentId
        {
            get;
            set;
        }
    }
}
