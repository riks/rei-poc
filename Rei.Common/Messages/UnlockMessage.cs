﻿using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("unlock_message")]
    public class UnlockMessage : Message
    {
        [JsonProperty(Required = Required.Always)]
        public long TaskId
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Alias
        {
            get;
            set;
        }
    }
}
