﻿using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("lock_request")]
    public class LockRequest : Request
    {
        [JsonProperty(Required = Required.Always)]
        public long TaskId
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Alias
        {
            get;
            set;
        }
    }
}
