﻿using System.Runtime.Serialization;

namespace Rei.Messages
{
    public enum MessageType
    {
        [EnumMember(Value = "hello_request")]
        HelloRequest,

        [EnumMember(Value = "hello_response")]
        HelloResponse,
    }
}
