﻿using System;

using Newtonsoft.Json;

namespace Rei.Messages
{
    class DateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            bool result = typeof(DateTimeOffset?).IsAssignableFrom(objectType);

            return result;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            double ticks;

            if (value is DateTimeOffset dateTimeOffset)
            {
                ticks = (double)dateTimeOffset.ToUnixTimeMilliseconds() / 1000;
            }
            else
            {
                throw new JsonSerializationException("Expected date object value.");
            }

            writer.WriteValue(ticks);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            bool nullable = !objectType.IsValueType;

            if (!nullable)
            {
                nullable = objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>);
            }

            if (reader.TokenType == JsonToken.Null)
            {
                if (!nullable)
                {
                    throw new JsonSerializationException(string.Format("Cannot convert null value to {0}.", objectType));
                }

                return null;
            }

            double ticks;

            if (reader.TokenType == JsonToken.Integer)
            {
                ticks = (long)reader.Value;
            }
            else if (reader.TokenType == JsonToken.Float)
            {
                ticks = (double)reader.Value;
            }
            else if (reader.TokenType == JsonToken.String)
            {
                if (!double.TryParse((string)reader.Value, out ticks))
                {
                    throw new JsonSerializationException(string.Format("Cannot convert invalid value to {0}.", objectType));
                }
            }
            else
            {
                throw new JsonSerializationException(string.Format("Unexpected token parsing date. Expected Float or String, got {0}.", reader.TokenType));
            }

            return DateTimeOffset.FromUnixTimeMilliseconds((long)(ticks * 1000)).ToLocalTime();
        }
    }
}
