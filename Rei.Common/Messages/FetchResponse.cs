﻿using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("fetch_response")]
    public class FetchResponse : Response
    {
        [JsonProperty(Required = Required.Always)]
        public long TaskId
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Alias
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string ContentKind
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Url
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public string ContentType
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public byte[] Content
        {
            get;
            set;
        }
    }
}
