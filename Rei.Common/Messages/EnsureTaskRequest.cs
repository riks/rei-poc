﻿using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("ensure_task_request")]
    public class EnsureTaskRequest : Request
    {
        [JsonProperty(Required = Required.Always)]
        public string Alias
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string ContentKind
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Url
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public long? ParentId
        {
            get;
            set;
        }
    }
}
