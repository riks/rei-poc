﻿using Newtonsoft.Json;

using Rei.Common;

namespace Rei.Messages
{
    public abstract class Response : Message
    {
        string statusText;

        [JsonProperty(Required = Required.Always)]
        public ushort StatusCode
        {
            get;
            set;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, Required = Required.DisallowNull)]
        public string StatusText
        {
            get => this.statusText ?? ((StatusCode)this.StatusCode).StatusText(false);
            set => this.statusText = value;
        }
    }
}
