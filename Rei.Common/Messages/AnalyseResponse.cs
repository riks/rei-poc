﻿using Newtonsoft.Json;

namespace Rei.Messages
{
    [Message("analyse_response")]
    public class AnalyseResponse : Response
    {
        [JsonProperty(Required = Required.Always)]
        public long TaskId
        {
            get;
            set;
        }

        [JsonProperty(Required = Required.Always)]
        public string Alias
        {
            get;
            set;
        }
    }
}
